#shader vertex
#version 330 core

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec2 aTexCoord;
layout(location = 2) in vec3 aNormal;

out vec3 vPosition;
out vec3 vNormal;

uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProjection;

void main()
{
	vPosition = vec3(uModel * vec4(aPosition, 1.0));
	vNormal = mat3(transpose(inverse(uModel))) * aNormal;

	gl_Position = uProjection * uView * uModel * vec4(aPosition, 1.0);
}

#shader pixel
#version 330 core

layout(location = 0) out vec4 colour;

in vec3 vPosition;
in vec3 vNormal;

uniform mat4 uModel;
uniform vec3 uCamPos;

struct DirLight
{
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct PointLight
{
	vec3 position;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant, linear, quadratic;
};

struct SpotLight
{
	vec3 position;
	vec3 direction;
	float cutOff;
	float outerCutOff;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant, linear, quadratic;
};

struct Surface
{
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

uniform Surface uSurface;

uniform DirLight uDLight;
#define NO_POINT_LIGHTS 2
uniform PointLight uPLights[NO_POINT_LIGHTS];
uniform SpotLight uSLight;

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.position - fragPos);

	// Ambient
	vec3 ambient = light.ambient * uSurface.diffuse;

	// Diffuse
	float diff = max(dot(normal, lightDir), 0.0);
	vec3 diffuse = light.diffuse * (diff * uSurface.diffuse);

	// Specular
	vec3 reflectDir = reflect(lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), uSurface.shininess);
	vec3 specular = light.specular * (spec * uSurface.specular);

	float distance = length(light.position - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

	ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;

	return ambient + diffuse + specular;
}

vec3 CalcDirectionalLight(DirLight light, vec3 normal, vec3 viewDir)
{
	vec3 lightDir = normalize(-light.direction);

	// Ambient
	vec3 ambient = light.ambient * uSurface.diffuse;

	// Diffuse
	float diff = max(dot(normal, lightDir), 0.0);
	vec3 diffuse = light.diffuse * (diff * uSurface.diffuse);

	// Specular
	vec3 reflectDir = reflect(lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), uSurface.shininess);
	vec3 specular = light.specular * (spec * uSurface.specular);

	return ambient + diffuse + specular;
}

vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	// No idea why I need to do this
	vec3 lPos = vec3(light.position.xy, -light.position.z);
	vec3 lightDir = normalize(lPos - fragPos);

	vec3 ambient = light.ambient * uSurface.diffuse;

	// Diffuse
	float diff = max(dot(normal, lightDir), 0.0);
	vec3 diffuse = light.diffuse * (diff * uSurface.diffuse);

	// Specular
	vec3 reflectDir = reflect(lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), uSurface.shininess);
	vec3 specular = light.specular * (spec * uSurface.specular);

	float distance = length(lPos - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

	float theta = dot(lightDir, normalize(-light.direction));
	float epsilon = light.cutOff - light.outerCutOff;
	float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
	
	// return specular;
	ambient *= attenuation * intensity;
	diffuse *= attenuation * intensity;
	specular *= attenuation * intensity;

	return ambient + diffuse + specular;
}

void main()
{
	vec3 normal = normalize(vNormal);
	vec3 viewDir = normalize(uCamPos - vPosition);

	vec3 res = CalcDirectionalLight(uDLight, normal, viewDir);

	for (int i = 0; i < NO_POINT_LIGHTS; i++)
		res += CalcPointLight(uPLights[i], normal, vPosition, viewDir);
	
	res += CalcSpotLight(uSLight, normal, vPosition, viewDir);

	colour = vec4(res, 1.0);
}
