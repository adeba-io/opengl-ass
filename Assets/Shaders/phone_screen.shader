#shader vertex
#version 330 core

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec2 aTexCoord;
layout(location = 2) in vec3 aNormal;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCoord;

uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProjection;
uniform vec3 uLightPos;

uniform float uTime;
#define SWIPE_GAP 9
#define SWIPE_DUR .1

float map(float value, float min1, float max1, float min2, float max2)
{
	return min2 + (value - min1) * (max2 - min2) / (max1 - min1);
}

float mod(float a, float b) { return a - b * floor(a / b); }

vec2 GetTexCoord()
{
	float time = mod(uTime, (SWIPE_GAP + SWIPE_DUR) * 3);
	const float disp = 0.25;

	if (time < SWIPE_GAP)
		return aTexCoord;
	else if (time < SWIPE_GAP + SWIPE_DUR)
	{
		time -= SWIPE_GAP;
		float offset = map(time, 0, SWIPE_DUR, 0, 1);
		return aTexCoord + vec2(offset * disp, 0.0);
	}
	else if (time < SWIPE_GAP * 2)
	{
		return aTexCoord + vec2(disp, 0.0);
	}
	else if (time < (SWIPE_GAP * 2 + SWIPE_DUR))
	{
		time -= SWIPE_GAP * 2;
		float offset = map(time, 0, SWIPE_DUR, 0, 1);
		return aTexCoord + vec2(offset * disp + disp, 0.0);
	}
	else if (time < SWIPE_GAP * 3)
	{
		return aTexCoord + vec2(disp * 2, 0.0);
	}

	time -= SWIPE_GAP * 3;
	time = SWIPE_DUR - time;
	float offset = map(time, 0, SWIPE_DUR, 0, disp * 2);
	return aTexCoord + vec2(offset, 0);
}

void main()
{
	vPosition = vec3(uModel * vec4(aPosition, 1.0));
	vNormal = mat3(transpose(inverse(uModel))) * aNormal;
	vTexCoord = GetTexCoord();

	gl_Position = uProjection * uView * uModel * vec4(aPosition, 1.0);
}

#shader pixel
#version 330 core

layout(location = 0) out vec4 colour;

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;

uniform mat4 uModel;
uniform vec3 uCamPos;

struct DirLight
{
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct PointLight
{
	vec3 position;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant, linear, quadratic;
};

struct SpotLight
{
	vec3 position;
	vec3 direction;
	float cutOff;
	float outerCutOff;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant, linear, quadratic;
};

struct Surface
{
	sampler2D texture1;
	vec3 specular;
	float shininess;
};

uniform Surface uSurface;

uniform DirLight uDLight;
#define NO_POINT_LIGHTS 2
uniform PointLight uPLights[NO_POINT_LIGHTS];
uniform SpotLight uSLight;

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.position - fragPos);
	vec3 tDiffuse = texture(uSurface.texture1, vTexCoord).rgb;

	// Ambient
	vec3 ambient = light.ambient * tDiffuse;

	// Diffuse
	float diff = max(dot(normal, lightDir), 0.0);
	vec3 diffuse = light.diffuse * (diff * tDiffuse);

	// Specular
	vec3 reflectDir = reflect(lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), uSurface.shininess);
	vec3 specular = light.specular * (spec * uSurface.specular);

	float distance = length(light.position - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

	ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;

	return ambient + diffuse + specular;
}

vec3 CalcDirectionalLight(DirLight light, vec3 normal, vec3 viewDir)
{
	vec3 lightDir = normalize(-light.direction);
	vec3 tDiffuse = texture(uSurface.texture1, vTexCoord).rgb;

	// Ambient
	vec3 ambient = light.ambient * tDiffuse;

	// Diffuse
	float diff = max(dot(normal, lightDir), 0.0);
	vec3 diffuse = light.diffuse * (diff * tDiffuse);

	// Specular
	vec3 reflectDir = reflect(lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), uSurface.shininess);
	vec3 specular = light.specular * (spec * uSurface.specular);

	return ambient + diffuse + specular;
}

vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lPos = vec3(light.position.xy, -light.position.z);
	vec3 lightDir = normalize(lPos - fragPos);
	vec3 tDiffuse = texture(uSurface.texture1, vTexCoord).rgb;

	vec3 ambient = light.ambient * tDiffuse;

	// Diffuse
	float diff = max(dot(normal, lightDir), 0.0);
	vec3 diffuse = light.diffuse * (diff * tDiffuse);

	// Specular
	vec3 reflectDir = reflect(lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), uSurface.shininess);
	vec3 specular = light.specular * (spec * uSurface.specular);

	float distance = length(lPos - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

	float theta = dot(lightDir, normalize(-light.direction));
	float epsilon = light.cutOff - light.outerCutOff;
	float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
	
	ambient *= attenuation * intensity;
	diffuse *= attenuation * intensity;
	specular *= attenuation * intensity;

	return ambient + diffuse + specular;
}

void main()
{
	vec3 normal = mat3(transpose(inverse(uModel))) * normalize(vNormal);
	vec3 viewDir = normalize(uCamPos - vPosition);

	vec3 res = CalcDirectionalLight(uDLight, normal, viewDir);

	for (int i = 0; i < NO_POINT_LIGHTS; i++)
		res += CalcPointLight(uPLights[i], normal, vPosition, viewDir);
	
	res += CalcSpotLight(uSLight, normal, vPosition, viewDir);

	// colour = vec4(CalcSpotLight(uSLight, normal, vPosition, viewDir), 1.0);
	colour = vec4(res, 1.0);
	// colour = vec4(vec3(uSLight.direction.x), 0);
	// colour = texture(uSurface.texture1, vTexCoord);
}
