#shader vertex
#version 330 core

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec2 aTexCoord;
layout(location = 2) in vec3 aNormal;

uniform mat4 uView;
uniform mat4 uProjection;

out vec3 vTexCoord;

void main()
{
	vTexCoord = aPosition;
	vec4 pos = uProjection * uView * vec4(aPosition, 1.0);
	gl_Position = pos.xyww;
}

#shader pixel
#version 330 core

layout(location = 0) out vec4 colour;

uniform samplerCube uSkyBox;

in vec3 vTexCoord;

void main()
{
	colour = texture(uSkyBox, vTexCoord);
}
