# Source Kit

Assignment for University of Sheffield COM3503 3D Computer Graphics

In order to compile you only need [MinGW](https://sourceforge.net/projects/mingw/) installed.
Run `first_time_compile.bat` to setup the environment for compiling or run `make_libraries.py` and `compile_pch.py`
then `compile.py`.

### Controls

Forward:                 W
Backward:                S
Left:                    A
Right:                   D
Up:                      Q
Down:                    E
Toggle Camera Control:   Z
Rotate Camera:           Mouse

*Note: the movement is relative to the world axis, ie. you won't move forward in the camera's forward direction*

These controls can be changed in the `Source/main.cpp` and `Source/Assets/Scripts/scr_camera.cpp` files. Recompile afterwards.

### Assets

In the `Assets` folder you will find all the assets used to compose this scene.
All textures must end in `png` and not have an alpha/transparency channel.
Only the scene.scn file is used. You must replace it with a different file if you want to use a different scene.
Only wavefront obj files are used. It is assumed they have positions, texture coordinates and normals, and that they are already triangulated.
Be careful when editing `.scn` and `.anim` files. There is no complex checking to ensure data isn't misread so make sure you follow the syntax.
