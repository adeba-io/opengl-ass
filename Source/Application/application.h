// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "layers"
#include "debug"
#include "events"
#include "util/singleton"

#include "Debug/imgui_layer.h"
#include "window.h"

class App : public MxnSingleton<App>
{
public:
	inline static bool IsRunning() { return Get()->_running; }
	inline static void SetRunning(bool value) { Get()->_running = value; }
	
	inline static bool IsUsingRobotCamera() { return Get()->_useRobotCamera; }
	inline static void ToggleUseRobotCamera() { Get()->_useRobotCamera = !Get()->_useRobotCamera; }

	inline static LayerStack& GetLayerStack() { return Get()->_layerStack; }

	inline static Window& GetWindow() { return *Get()->_window; }

	inline static ImGUILayer& GetImGUILayer() { return *Get()->_ImGUILayer; }

	inline static void ToggleMouseCapture() { Get()->_window->ToggleMouseCapture(); }
	inline static bool IsMouseCaptured() { return Get()->_window->IsMouseCaptured(); }

	static void Init() { new App; }


private:
	App()
		: _running(false),_useRobotCamera(false), _layerStack(), _window(nullptr), _ImGUILayer(new ImGUILayer), MxnSingleton<App>(this)
	{
		_window = new Window();
		_layerStack.PushOverlay(_ImGUILayer);
	}
	
	bool _running;
	bool _useRobotCamera;
	LayerStack _layerStack;

	Window *_window;
	ImGUILayer *_ImGUILayer; 
};
