// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "layer.h"

#ifdef BLD_DEBUG
MxnLayer::MxnLayer(const std::string& name) : _debugName(name)
#else
MxnLayer::MxnLayer()
#endif
{}

MxnLayer::~MxnLayer() {}