// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include <GLFW/glfw3.h>
#include "core"
#include "events"

#include "Graphics/Renderer/context.h"

struct WindowProps
{
	std::string title;
	unsigned int width, height;

	WindowProps(
		const std::string& title = "OpenGL Showcase", 
		unsigned int width = 1280,
		unsigned int height = 720) 
		: title(title), width(width), height(height)
	{}
};

class Window
{
public:

	Window(const WindowProps& props = WindowProps());
	~Window();

	void OnUpdate();

	inline unsigned int GetWidth() const { return _data.width; }
	inline unsigned int GetHeight() const { return _data.height; }

	using FnEventCallback = std::function<void(MxnEvent&)>;

	// Window Attributes
	inline void SetEventCallback(const FnEventCallback& callback) { _data.callback = callback; }
	void SetVSync(bool enabled);
	bool IsVSync() const;
	void* GetNativeWindow() const;

	void ToggleMouseCapture();
	inline bool IsMouseCaptured() const { return _mouseCaptured; }

private:
	void Init(const WindowProps& props);
	void Shutdown();

private:
	GLFWwindow *_window;
	RenderContext *_renderContext;
	bool _mouseCaptured;

	// We have this struct so we can send this data to GLFW
	// without sending the whole class
	struct WindowData
	{
		std::string title;
		unsigned int width, height;
		bool vSync;

		FnEventCallback callback;
	};

	WindowData _data;
};
