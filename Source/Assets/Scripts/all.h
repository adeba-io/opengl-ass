// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "scr_camera.h"
#include "scr_spotlight.h"
#include "scr_light_control.h"
#include "scr_robot.h"

#define SCRIPT(script) { #script, []() -> MxnScript* { return (MxnScript*)new Scr##script; } }

const std::unordered_map<std::string, MxnScript* (*)()> g_scripts =
{
	SCRIPT(FreeCamera),
	SCRIPT(SpotLight),
	SCRIPT(LightControl), 
	SCRIPT(Robot)
};

#undef SCRIPT
