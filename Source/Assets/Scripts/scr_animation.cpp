// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "scr_animation.h"
#include "nodetree"
#include "gameloop"
void ScrAnimation::Int_Begin()
{
	switch (_self->GetTransformationType())
	{
		case TransformationType::Translate:
		{
			NdTranslate *t = static_cast<NdTranslate*>(_self);
			_vSelf = static_cast<MxnVector*>(t);
			break;
		}
		case TransformationType::Rotate:
		{
			NdRotate *r = static_cast<NdRotate*>(_self);
			_vSelf = static_cast<MxnVector*>(r);
			break;
		}
		case TransformationType::Scale:
		{
			NdScale *s = static_cast<NdScale*>(_self);
			_vSelf = static_cast<MxnVector*>(s);
			break;
		}
	}

	// LG_INF("Keyframes for {}:", _self->GetName());
	// for (auto keyframe : _keyframes)
	// 	LG_INF("  {} : V ({}, {}, {})", keyframe.name, keyframe.vector.x, keyframe.vector.y, keyframe.vector.z);
}

glm::vec3 lerp(glm::vec3 from, glm::vec3 to, float time);

void ScrAnimation::Process()
{
	if (!_animate) return;

	const float duration = 4.0f;
	float offset = Time::GetTime() - _startTime;

	if (offset > duration)
	{
		_iCurrentKeyFrame = ++_iCurrentKeyFrame % _keyframes.size();
		_animate = _iCurrentKeyFrame != _iTargetKeyFrame;
		return;
	}

	float time = offset / duration;
	glm::vec3 newVec = lerp(
		_keyframes[_iCurrentKeyFrame].vector,
		_keyframes[_iTargetKeyFrame].vector,
		time);

	_vSelf->vector = newVec;
}

void ScrAnimation::SetCurrentPose(std::string name)
{
	for (unsigned int i = 0; i < _keyframes.size(); i++)
	{
		if (_keyframes[i].name != name)
			continue;
		
		_startTime = Time::GetTime();
		// _iCurrentKeyFrame = _iTargetKeyFrame;
		_iTargetKeyFrame = i;
		_animate = _iTargetKeyFrame != _iCurrentKeyFrame;
	}
}

float lerp(float from, float to, float time) { return from + (to - from) * time; }

glm::vec3 lerp(glm::vec3 from, glm::vec3 to, float time)
{
	return glm::vec3(lerp(from.x, to.x, time), lerp(from.y, to.y, time), lerp(from.z, to.z, time));
}
