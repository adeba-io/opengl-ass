// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include <glm/glm.hpp>
#include "core"
#include "nodetree"
#include "script.h"

struct KeyFrame
{
	std::string name;
	bool isInbetweener;
	glm::vec3 vector;
};

class ScrAnimation : public MxnScript
{
public:
	ScrAnimation(std::vector<KeyFrame>& keyframes)
		: _vSelf(nullptr), _keyframes(keyframes),
		  _animate(false), _iCurrentKeyFrame(0), _iTargetKeyFrame(0), _startTime(0.f) {}
	
	void Int_Begin() override;
	void Process() override;
	void ImGuiProcess() override {}

	void SetCurrentPose(std::string name);

private:
	MxnVector *_vSelf;
	std::vector<KeyFrame> _keyframes;

	bool _animate;
	unsigned int _iCurrentKeyFrame, _iTargetKeyFrame;
	float _startTime;
};

