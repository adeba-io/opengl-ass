// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "scr_camera.h"

#include <glm/glm.hpp>
#include <GLFW/glfw3.h>

#include "app"
#include "nodetree"
#include "gameloop"
#include "input"
#include "debug"

#define PRESSED(x) if (IsPressed(MngInput::GetKeyState(x)))

void Translation(MxnNode *self);
void Rotation(MxnNode *self);

void ScrFreeCamera::Int_Begin()
{
	if (_self->GetTransformationType() == TransformationType::Translate)
		_Process = Translation;
	else if (_self->GetTransformationType() == TransformationType::Rotate)
		_Process = Rotation;
	else
		LG_ERR("{}'s FreeCamera script not assigned", _self->GetName());
}

void Translation(MxnNode *self)
{
	NdTranslate *pos = static_cast<NdTranslate*>(self);
	const float speed = 5.0f;

	PRESSED(GLFW_KEY_Q) pos->vector.y += speed * Time::GetDeltaTime();
	else PRESSED(GLFW_KEY_E) pos->vector.y -= speed * Time::GetDeltaTime();

	PRESSED(GLFW_KEY_D) pos->vector.x -= speed * Time::GetDeltaTime();
	else PRESSED(GLFW_KEY_A) pos->vector.x += speed * Time::GetDeltaTime();

	PRESSED(GLFW_KEY_W) pos->vector.z += speed * Time::GetDeltaTime();
	else PRESSED(GLFW_KEY_S) pos->vector.z -= speed * Time::GetDeltaTime();
}

void Rotation(MxnNode *self)
{
	NdRotate *rotate = static_cast<NdRotate*>(self);

	if (!App::IsMouseCaptured()) return;

	const float speed = 10.0f;
	
	const glm::vec2& mouseMove = MngInput::GetMouseMovement();
	rotate->vector.x -= mouseMove.y * Time::GetDeltaTime();
	rotate->vector.y -= mouseMove.x * Time::GetDeltaTime();
}
