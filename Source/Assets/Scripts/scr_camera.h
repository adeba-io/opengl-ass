// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "script.h"

typedef void (*FnScript)(MxnNode*);

class ScrFreeCamera : public MxnScript
{
public:
	void Int_Begin() override;
	void Process() override { _Process(_self); }
	void ImGuiProcess() override {}

private:
	FnScript _Process;
};
