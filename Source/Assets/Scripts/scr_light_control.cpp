// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "scr_light_control.h"

#include <imgui.h>

void ScrLightControl::Int_Begin() { _lSelf = static_cast<NdLight*>(_self); }

void ScrLightControl::ImGuiProcess()
{
	std::string label = _lSelf->GetName() + std::string(" Active");
	ImGui::Checkbox(label.c_str(), &_lSelf->on);
}
