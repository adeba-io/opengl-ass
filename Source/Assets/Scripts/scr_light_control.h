// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "script.h"
#include "debug"
#include "NodeTree/light_node.h"

class ScrLightControl : public MxnScript
{
public:
	void Int_Begin() override;
	void Process() override {}
	void ImGuiProcess() override;

private:
	NdLight *_lSelf;
};
