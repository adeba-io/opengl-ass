// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "scr_robot.h"

#include <imgui.h>
#include "core"
#include "app"
#include "nodetree"
#include "debug"

#include "Util/strings.h"

// A map of the animateable node to its keyframes
using MapAnimateable_KFrames = std::unordered_map<std::string, std::vector<KeyFrame>>;

void ReadKeyFrame(std::string keyframeName, std::ifstream& file, MapAnimateable_KFrames& mapAnimKFrames);

void ScrRobot::Int_Begin()
{
	std::ifstream fAnim(_self->GetAnimPath());
	std::string line;

	MapAnimateable_KFrames mapAnimKFrames;
	std::vector<std::string> allAnimatedNodes;
	bool first = true;

	while (getline(fAnim, line))
	{
		TrimLine(line);
		if (line.find("keyframe ") != 0)
			continue;
		
		line.erase(0, 9);

		ReadKeyFrame(line, fAnim, mapAnimKFrames);

		if (!first) continue;

		first = false;
		for (auto it = mapAnimKFrames.begin(); it != mapAnimKFrames.end(); it++)
			allAnimatedNodes.push_back(it->first);
	}

	for (auto it = mapAnimKFrames.begin(); it != mapAnimKFrames.end(); it++)
	{
		// LG_INF("Animateable {} Count {}", it->first, it->second.size());
		MxnNode *ndAnimate = FindByNameInTree(*_self, it->first);
		if (ndAnimate == nullptr)
		{
			LG_WRN("Node of name {} is not a child of {}", it->first, _self->GetName());
			continue;
		}

		ScrAnimation *scrAnim = new ScrAnimation(it->second);
		scrAnim->Begin(ndAnimate);
		ndAnimate->AddScript(scrAnim);
		_animationScripts.push_back(scrAnim);

		for (KeyFrame& keyframe : it->second)
		{
			if (keyframe.name == "") continue;
			auto it = std::find(_poseNames.begin(), _poseNames.end(), keyframe.name);
			if (it == _poseNames.end())
				_poseNames.push_back(keyframe.name);
		}
	}
}

void ScrRobot::Process()
{

}

void ScrRobot::ImGuiProcess()
{
	if (ImGui::Button(App::IsUsingRobotCamera() ? "Use Free Camera" : "Use Robot Camera"))
		App::ToggleUseRobotCamera();
	
	ImGui::Separator();
	unsigned int index = _poseNames.size();
	for (unsigned int i = 0; i < _poseNames.size(); i++)
	{
		if (!ImGui::Button(_poseNames[i].c_str())) continue;

		index = i;
		break;
	}

	if (index != _poseNames.size())
	{
		for (auto scrAnim : _animationScripts)
			scrAnim->SetCurrentPose(_poseNames[index]);
	}
}

KeyFrame ParseKeyFrameData(std::ifstream& file);

void ReadKeyFrame(std::string keyframeName, std::ifstream& file, MapAnimateable_KFrames& mapAnimKFrames)
{
	bool inbetweener = false;

	std::string line;
	while (getline(file, line))
	{
		TrimLine(line);

		if (line.find("#") == 0) continue;
		else if (line.find("/") == 0) return;
		else if (line.find("inbetweener ") == 0)
		{
			std::vector<std::string> split = SplitString(line, ' ');
			if (split.size() > 1)
				inbetweener = split[1] == "yes";
			else
				LG_WRN("Inbetweener flag on keyframe {} not set", keyframeName);
			continue;
		}
		else if (line.find("for ") == 0)
		{
			line.erase(0, 4);
			KeyFrame frame = ParseKeyFrameData(file);
			frame.name = keyframeName;
			frame.isInbetweener = inbetweener;

			auto it = mapAnimKFrames.find(line);
			if (it == mapAnimKFrames.end())
			{
				std::vector<KeyFrame> frames = { frame };
				mapAnimKFrames.emplace(line, frames);
			}
			else
			{
				it->second.push_back(frame);
				continue;
			}
		}
	}
}

KeyFrame ParseKeyFrameData(std::ifstream& file)
{
	KeyFrame frame;
	std::string line;
	while (getline(file, line))
	{
		TrimLine(line);
		if (line.find("#") == 0) continue;
		else if (line.find("/") == 0) return frame;
		else if (line.find("v ") == 0)
		{
			std::vector<std::string> split =  SplitString(line, ' ');
			glm::vec3 v(0.f, 0.f, 0.f);
			v.x = std::stof(split[1]);
			v.y = std::stof(split[2]);
			v.z = std::stof(split[3]);
			frame.vector = v;
		}
	}

	return frame;
}

