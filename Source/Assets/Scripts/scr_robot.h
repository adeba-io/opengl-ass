// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "core"
#include "nodetree"
#include "script.h"
#include "scr_animation.h"

class ScrRobot : public MxnScript
{
public:
	void Int_Begin() override;
	void Process() override;
	void ImGuiProcess() override;
	
private:
	std::vector<std::string> _poseNames;
	std::vector<ScrAnimation*> _animationScripts;
};
