// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "scr_spotlight.h"

#include <glm/glm.hpp>
#include "gameloop"

void ScrSpotLight::Int_Begin() { _rSelf = static_cast<NdRotate*>(_self); }

void ScrSpotLight::Process()
{
	const float speed = 2.f;
	const float amplitude = 30.f;

	float y = glm::sin(Time::GetTime() * speed) * amplitude;

	_rSelf->vector.y = y;
}
