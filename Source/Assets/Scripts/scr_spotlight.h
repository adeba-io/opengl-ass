// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "nodetree"
#include "script.h"

class ScrSpotLight : public MxnScript
{
public:
	void Int_Begin() override;
	void Process() override;
	void ImGuiProcess() override {}

private:
	NdRotate *_rSelf;
};
