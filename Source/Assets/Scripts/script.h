// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "NodeTree/node.h"

class MxnScript
{
public:
	void Begin(MxnNode *self) { _self = self; Int_Begin(); }
	virtual void Process() = 0;
	virtual void ImGuiProcess() = 0;

protected:
	virtual void Int_Begin() = 0;

	MxnNode *_self;
};
