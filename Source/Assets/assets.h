// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "core"

// For hard coded assets
#define AssetPath(x) fs::path("Assets") / #x
// For use with strings
#define Asset(x) fs::path("Assets") / x
