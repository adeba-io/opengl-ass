// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "model_reader.h"
#include "core"
#include "math"
#include "debug"

#include "Util/strings.h"

void ParseModel(const fs::path& path, VertexArray **vertexArray, VertexBuffer **vertexBuffer, IndexBuffer **indexBuffer)
{
	std::ifstream src(path.string());

	std::string line;
	std::vector<glm::vec3> positions, normals;
	std::vector<glm::vec2> texCoords;

	std::vector<glm::ivec3> dataIndicies;
	std::vector<unsigned int> indicies;

	while (getline(src, line))
	{
		if (line.find("#") == 0) // Wavefront comment
			continue;
		
		if (line.find("v ") == 0)
		{
			line.erase(0, 2);
			std::vector<std::string> split = SplitString(line, ' ');

			glm::vec3 pos;
			for (int i = 0; i < split.size(); i++)
				(&pos.x)[i] = std::stof(split[i]);
			
			positions.push_back(pos);
		}
		
		else if (line.find("vt ") == 0)
		{
			line.erase(0, 3);
			std::vector<std::string> split = SplitString(line, ' ');

			glm::vec2 uv;
			uv.x = std::stof(split[0]);
			uv.y = std::stof(split[1]);
			
			texCoords.push_back(uv);
		}
		else if (line.find("vn ") == 0)
		{
			line.erase(0, 3);
			std::vector<std::string> split = SplitString(line, ' ');

			glm::vec3 vertNormal;
			for (int i = 0; i < split.size(); i++)
				(&vertNormal.x)[i] = std::stof(split[i]);

			normals.push_back(vertNormal);
		}
		else if (line.find("f ") == 0)
		{
			line.erase(0, 2);
			std::vector<std::string> split = SplitString(line, ' ');

			for (std::string& face : split)
			{
				std::vector<std::string> indexSplit = SplitString(face, '/');
				// Assuming all vertices use a position, a normal, and a tex coord
				int position = std::stoi(indexSplit[0]) - 1;
				int uv = std::stoi(indexSplit[1]) - 1;
				int normal = std::stoi(indexSplit[2]) - 1;
				glm::ivec3 vec = { position, uv, normal };
				auto it = std::find(dataIndicies.begin(), dataIndicies.end(), vec);

				if (it == dataIndicies.end())
				{
					dataIndicies.push_back(vec);
					indicies.push_back(dataIndicies.size() - 1);
				}
				else
				{
					indicies.push_back(it - dataIndicies.begin());
				}
			}
		}
	}

	std::vector<float> verticies(dataIndicies.size() * (3 + 2 + 3));
	
	{
		int i = 0;
		for (auto& dI : dataIndicies)
		{
			int posID = dI.x;
			int uvID = dI.y;
			int normalID = dI.z;

			glm::vec3 pos = positions[posID];
			verticies[i++] = pos.x;
			verticies[i++] = pos.y;
			verticies[i++] = pos.z;

			glm::vec2& uv = texCoords[uvID];
			verticies[i++] = uv.x;
			verticies[i++] = uv.y;
			
			glm::vec3& norm = normals[normalID];
			verticies[i++] = norm.x;
			verticies[i++] = norm.y;
			verticies[i++] = norm.z;
		}
	}
	VertexBuffer *vb = new VertexBuffer(verticies.data(), verticies.size() * sizeof(float));
	vb->SetBufferLayout({
		{ ShaderDataType::Float3, "aPosition" },
		{ ShaderDataType::Float2, "aTexCoord" },
		{ ShaderDataType::Float3, "aNormal" }
	});

	IndexBuffer *ib = new IndexBuffer(indicies.data(), indicies.size());

	VertexArray *va = new VertexArray();
	va->AddVertexBuffer(vb);
	va->SetIndexBuffer(ib);

	*vertexBuffer = vb;
	*indexBuffer = ib;
	*vertexArray = va;

	src.close();
}
