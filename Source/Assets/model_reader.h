// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "core"
#include "Graphics/Data/vertex_array.h"
#include "Graphics/Data/buffers.h"

void ParseModel(const fs::path& path, VertexArray **vertexArray, VertexBuffer **vertexBuffer, IndexBuffer **indexBuffer);
