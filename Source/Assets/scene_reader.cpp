// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "scene_reader.h"

#include <glm/glm.hpp>
#include "debug"
#include "scripts"

#include "assets.h"
#include "NodeTree/camera_node.h"
#include "NodeTree/mesh_node.h"
#include "NodeTree/light_node.h"
#include "Util/strings.h"
#include "Graphics/Data/Material/mat_cubemap.h"

MxnNode* ReadNode(std::ifstream& file, std::string& type);
MxnNode* ReadPlainNode(std::ifstream& file);
MxnNode* ReadTranslateNode(std::ifstream& file);
MxnNode* ReadRotateNode(std::ifstream& file);
MxnNode* ReadScaleNode(std::ifstream& file);
MxnNode* ReadMeshNode(std::ifstream& file);
MxnNode* ReadCubeMapNode(std::ifstream& file);
MxnNode* ReadCameraNode(std::ifstream& file);
MxnNode* ReadLightNode(std::ifstream& file);

std::vector<MxnNode*> ReadChildren(std::ifstream& file);
void ReadScripts(std::string& scriptsSrc, std::vector<MxnScript*>& scripts);

MxnNode* ReadSceneData(const fs::path& sceneFile)
{
	std::ifstream scene(sceneFile);

	std::string line;
	while (getline(scene, line))
	{
		TrimLine(line);
		if (line.find("#") != 0)
			break;
	}

	line.erase(0, 5);
	MxnNode *root = ReadNode(scene, line);

	scene.close();
	return root;
}

MxnNode* ReadNode(std::ifstream& file, std::string& type)
{
	if (type.find("plain") == 0)
		return ReadPlainNode(file);
	if (type.find("translate") == 0)
		return ReadTranslateNode(file);
	if (type.find("rotate") == 0)
		return ReadRotateNode(file);
	if (type.find("scale") == 0)
		return ReadScaleNode(file);
	if (type.find("mesh") == 0)
		return ReadMeshNode(file);
	if (type.find("camera") == 0)
	{
		MxnNode *node = ReadCameraNode(file);
		return node;
	}
	if (type.find("light") == 0)
		return ReadLightNode(file);
	if (type.find("cubemap") == 0)
		return ReadCubeMapNode(file);
	
	return nullptr;
}

#define COMMON_VARS std::string name; fs::path animPath; std::vector<MxnNode*> children; std::vector<MxnScript*> scripts;

#define COMMON_PARSNG \
		if (line.find("#") == 0) continue; \
		if (line.find("/node") == 0) break;\
		if (line.find("name ") == 0)\
		{ \
			line.erase(0, 5);\
			name = std::string(line);\
		}\
		else if (line.find("anim ") == 0)\
		{\
			line.erase(0, 5);\
			animPath = fs::path(Asset(("Animations\\" + line + ".anim")));\
		}\
		else if (line.find("children") == 0) { children = ReadChildren(file); } \
		else if (line.find("scripts ") == 0) { line.erase(0, 8); ReadScripts(line, scripts); }

#define CHILDREN_AND_SCRIPTS \

MxnNode* ReadPlainNode(std::ifstream& file)
{
	COMMON_VARS

	std::string line;
	while (getline(file, line))
	{
		TrimLine(line);
		if (line.find("#") == 0) continue;

		COMMON_PARSNG
	}
	return new NdPlain(name.c_str(), animPath, children, scripts);
}

MxnNode* ReadTranslateNode(std::ifstream& file)
{
	COMMON_VARS
	glm::vec3 vec;

	std::string line;
	while (getline(file, line))
	{
		TrimLine(line);
		
		COMMON_PARSNG
		else if (line.find("v ") == 0)
		{
			line.erase(0, 2);
			std::vector<std::string> split = SplitString(line, ' ');
			vec.x = std::stof(split[0]);
			vec.y = std::stof(split[1]);
			vec.z = std::stof(split[2]);
		}
	}

	return new NdTranslate(name.c_str(), vec, animPath, children, scripts);
}

MxnNode* ReadRotateNode(std::ifstream& file)
{
	COMMON_VARS
	glm::vec3 vec;
	RotationOrder order;

	std::string line;
	while (getline(file, line))
	{
		TrimLine(line);
		
		COMMON_PARSNG
		else if (line.find("v ") == 0)
		{
			line.erase(0, 2);
			std::vector<std::string> split = SplitString(line, ' ');
			vec.x = std::stof(split[0]);
			vec.y = std::stof(split[1]);
			vec.z = std::stof(split[2]);
		}
		else if (line.find("order ") == 0)
		{
			line.erase(0, 6);
			order = RotOrderFromString(line);
		}
	}

	return new NdRotate(name.c_str(), vec, order, animPath, children, scripts);
}

MxnNode* ReadScaleNode(std::ifstream& file)
{
	COMMON_VARS
	glm::vec3 vec(1.0f, 1.0f, 1.0f);

	std::string line;
	while (getline(file, line))
	{
		TrimLine(line);
		
		COMMON_PARSNG
		else if (line.find("v ") == 0)
		{
			line.erase(0, 2);
			std::vector<std::string> split = SplitString(line, ' ');
			vec.x = std::stof(split[0]);
			vec.y = std::stof(split[1]);
			vec.z = std::stof(split[2]);
		}
	}

	return new NdScale(name.c_str(), vec, animPath, children, scripts);
}

MxnNode* ReadMeshNode(std::ifstream& file)
{
	COMMON_VARS

	fs::path modelPath;
	fs::path shaderPath;
	std::vector<fs::path> texturePaths;
	glm::vec3 diffuse(0.6f, 0.6f, 0.6f);
	glm::vec3 specular(0.6f, 0.6f, 0.6f);
	float shininess = 32.0f;

	std::string line;
	while (getline(file, line))
	{
		TrimLine(line);
		
		COMMON_PARSNG
		else if (line.find("mesh ") == 0)
		{
			line.erase(0, 5);
			line += ".obj";
			modelPath = Asset(("Models\\" + line));
		}
		else if (line.find("tex ") == 0)
		{
			line.erase(0, 4);
			std::vector<std::string> split = SplitString(line, ',');
			for (auto it = split.begin(); it !=  split.end(); it++)
			{
				TrimLine(*it);
				texturePaths.push_back(Asset(("Textures\\" + *it + ".png")));
			}
		}
		else if (line.find("shader ") == 0)
		{
			line.erase(0, 7);
			line += ".shader";
			shaderPath = Asset(("Shaders\\" + line));
		}
		else if (line.find("diffuse ") == 0)
		{
			std::vector<std::string> split = SplitString(line, ' ');
			diffuse.x = std::stof(split[1]);
			diffuse.y = std::stof(split[2]);
			diffuse.z = std::stof(split[3]);
		}
		else if (line.find("specular ") == 0)
		{
			std::vector<std::string> split = SplitString(line, ' ');
			specular.x = std::stof(split[1]);
			specular.y = std::stof(split[2]);
			specular.z = std::stof(split[3]);
		}
		else if (line.find("shininess ") == 0)
		{
			std::vector<std::string> split = SplitString(line, ' ');
			shininess = std::stof(split[1]);
		}
	}

	return new NdMesh(name.c_str(), modelPath, GetAppropriateMaterial(shaderPath, texturePaths, diffuse, specular, shininess), animPath, children, scripts);
}

MxnNode* ReadCubeMapNode(std::ifstream& file)
{
	COMMON_VARS

	fs::path modelPath;
	fs::path shaderPath;
	std::vector<fs::path> facePaths;


	std::string line;
	while (getline(file, line))
	{
		TrimLine(line);
		
		COMMON_PARSNG
		else if (line.find("mesh ") == 0)
		{
			line.erase(0, 5);
			line += ".obj";
			modelPath = Asset(("Models\\" + line));
		}
		else if (line.find("shader ") == 0)
		{
			line.erase(0, 7);
			line += ".shader";
			shaderPath = Asset(("Shaders\\" + line));
		}
		else if (line.find("tex ") == 0)
		{
			std::string filename = SplitString(line, ' ')[1];
			std::string suffixes[6] = { "right", "left", "top", "bottom", "front", "back" };
			for (unsigned int i = 1; i < 6; i++)
			{
				std::string face = filename + "_" + suffixes[i - 1] + ".png";
				facePaths.push_back(Asset(("Cubemap\\" + face)));
			}
		}
	}

	return new NdMesh(name.c_str(), modelPath, new MatCubeMap(shaderPath, facePaths), animPath, children, scripts);
}

MxnNode* ReadCameraNode(std::ifstream& file)
{
	COMMON_VARS

	float fov = 45.f;
	float aspect = 1280.f / 720.f;
	float near = 1.f;
	float far = 300.f;

	std::string line;
	while (getline(file, line))
	{
		TrimLine(line);
		
		COMMON_PARSNG
		else if (line.find("fov ") == 0)
		{
			line.erase(0, 4);
			fov = std::stof(line);
		}
		else if (line.find("aspect ") == 0)
		{
			line.erase(0, 7);
			std::vector<std::string> split = SplitString(line, ' ');
			if (split.size() == 2)
				aspect = std::stof(split[0]) / std::stof(split[1]);
			else
				aspect = std::stof(line);
		}
		else if (line.find("near ") == 0)
		{
			line.erase(0, 5);
			near = std::stof(line);
		}
		else if (line.find("far ") == 0)
		{
			line.erase(0, 4);
			far = std::stof(line);
		}
	}

	return new NdCamera(name.c_str(), fov, aspect, near, far, animPath, children, scripts);
}

MxnNode* ReadLightNode(std::ifstream& file)
{
	COMMON_VARS
	LightType type = LightType::Position;
	glm::vec3 direction(0.f, -1.f, 0.f);
	bool on = true;

	glm::vec3 ambient(1.f, 1.f, 1.f);
	glm::vec3 diffuse(1.f, 1.f, 1.f);
	glm::vec3 specular(1.f, 1.f, 1.f);

	float constant = 1.0f;
	float linear = 0.09f;
	float quadratic = 0.032f;
	float intensity = 1.f;

	float cutOff = 12.5f;
	float outerCutOff = 20.0f;


	std::string line;
	while (getline(file, line))
	{
		TrimLine(line);
		
		COMMON_PARSNG
		else if (line.find("on ") == 0)
		{
			std::vector<std::string> split = SplitString(line, ' ');
			on = std::stoi(split[1]) != 0;
		}
		else if (line.find("type ") == 0)
		{
			line.erase(0, 5);
			type = LTypeFromString(line);
		}
		else if (line.find("dir ") == 0)
		{
			std::vector<std::string> split = SplitString(line, ' ');
			direction.x = std::stof(split[1]);
			direction.y = std::stof(split[2]);
			direction.z = std::stof(split[3]);
		}
		else if (line.find("ambient ") == 0)
		{
			std::vector<std::string> split = SplitString(line, ' ');
			ambient.x = std::stof(split[1]);
			ambient.y = std::stof(split[2]);
			ambient.z = std::stof(split[3]);
		}
		else if (line.find("diffuse ") == 0)
		{
			std::vector<std::string> split = SplitString(line, ' ');
			diffuse.x = std::stof(split[1]);
			diffuse.y = std::stof(split[2]);
			diffuse.z = std::stof(split[3]);
		}
		else if (line.find("specular ") == 0)
		{
			std::vector<std::string> split = SplitString(line, ' ');
			specular.x = std::stof(split[1]);
			specular.y = std::stof(split[2]);
			specular.z = std::stof(split[3]);
		}
		else if (line.find("constant ") == 0)
		{
			std::vector<std::string> split = SplitString(line, ' ');
			constant = std::stof(split[1]);
		}
		else if (line.find("linear ") == 0)
		{
			std::vector<std::string> split = SplitString(line, ' ');
			linear = std::stof(split[1]);
		}
		else if (line.find("quadratic ") == 0)
		{
			std::vector<std::string> split = SplitString(line, ' ');
			quadratic = std::stof(split[1]);
		}
		else if (line.find("cutoff ") == 0)
		{
			std::vector<std::string> split = SplitString(line, ' ');
			cutOff = std::stof(split[1]);
		}
		else if (line.find("outcutoff ") == 0)
		{
			std::vector<std::string> split = SplitString(line, ' ');
			outerCutOff = std::stof(split[1]);
		}
		else if (line.find("intensity ") == 0)
		{
			std::vector<std::string> split = SplitString(line, ' ');
			intensity = std::stof(split[1]);
		}
	}

	return new NdLight(
		name.c_str(), type, on,
		direction, ambient, diffuse, specular, intensity,
		constant, linear, quadratic,
		cutOff, outerCutOff,
		animPath, children, scripts);
}

std::vector<MxnNode*> ReadChildren(std::ifstream& file)
{
	std::vector<MxnNode*> children;
	std::string line;
	
	while (getline(file, line))
	{
		TrimLine(line);
		if (line.find("#") == 0) continue;

		if (line.find("/children") == 0)
			break;
		
		if (line.find("node ") == 0)
		{
			line.erase(0, 5);
			MxnNode *child = ReadNode(file, line);
			if (child)
				children.push_back(child);
		}
	}

	return children;
}

void ReadScripts(std::string& scriptsSrc, std::vector<MxnScript*>& scripts)
{
	std::vector<std::string> split = SplitString(scriptsSrc, ',');
	for (unsigned int i = 0; i < split.size(); i++)
	{
		std::string& scr = split[i];
		TrimLine(scr);
		auto it = g_scripts.find(scr);
		if (it == g_scripts.end())
		{
			LG_WRN("Script {} not found", scr);
			continue;
		}

		scripts.push_back(it->second());
	}
}
