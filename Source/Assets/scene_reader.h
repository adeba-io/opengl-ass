// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "nodetree"
#include "core"

MxnNode* ReadSceneData(const fs::path& sceneFile);
