// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "layers"
#include "nodetree"

class ImGUILayer : public MxnLayer
{
public:
	ImGUILayer();
	~ImGUILayer();

	void OnAttach() override;
	void OnDetach() override;
	void OnImGUIRender() override;

	void Begin();
	void End();

	MxnNode* treeRoot;

private:
	float _time = 0.0f;
};
