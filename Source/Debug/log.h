// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include <spdlog/spdlog.h>
#include <spdlog/fmt/ostr.h>

class Log
{
public:
	static void Init();

	inline static spdlog::logger* GetGeneral() { return sGeneralLogger; }

private:
	static spdlog::logger *sGeneralLogger;
};
