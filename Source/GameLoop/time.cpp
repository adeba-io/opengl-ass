// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "time.h"
#include <GLFW/glfw3.h>

void Time::UpdateImpl()
{
	_lastFrameTime = _currentFrameTime;
	_currentFrameTime = (float)glfwGetTime();
	_deltaTime = _currentFrameTime - _lastFrameTime;
}
