// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once
#include "debug"
#include "util/singleton"

class Time : public MxnSingleton<Time>
{
public:
	inline static void Update() { Get()->UpdateImpl(); }

	inline static float GetDeltaTime() { return Get()->_deltaTime; }
	inline static float GetTime() { return Get()->_currentFrameTime; }

	static void Init() { new Time; }

private:
	Time() : MxnSingleton<Time>(this) {}

	void UpdateImpl();

	float _deltaTime = 0.0f;

	float _currentFrameTime = 0.0f;
	float _lastFrameTime = 0.0f;
};
