// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "material.h"

class MatComplete : public MxnMaterial
{
public:
	MatComplete(const fs::path& shaderPath, const glm::vec3& diffuse, const glm::vec3& specular, float shininess);
	~MatComplete() override;
 
	void Bind() const override;
	void UnBind() const override;
	
	void UploadData(const glm::mat4& model, const glm::mat4& view, const glm::mat4& projection) override;

private:
	glm::vec3 _diffuse;
	glm::vec3 _specular;
	float _shininess;
};
