// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "mat_cubemap.h"

#include <glad/glad.h>
#include "debug"
#include "Assets/stb_image.h"
#include "Assets/assets.h"

MatCubeMap::MatCubeMap(const fs::path& shaderPath, const std::vector<fs::path>& facePaths)
	: _cubemapID(0), MxnMaterial(shaderPath)
{
	glGenTextures(1, &_cubemapID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, _cubemapID);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	int width, height, nrChannels;
	for (unsigned int i = 0; i < facePaths.size(); i++)
	{
		unsigned char *data = stbi_load(facePaths[i].string().c_str(), &width, &height, &nrChannels, 0);
		if (data)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		}
		else
		{
			LG_ERR("Cube map texture failed to load at path: {}", facePaths[i]);
		}

		stbi_image_free(data);
	}
}

MatCubeMap::~MatCubeMap()
{
	delete _shader;
}

void MatCubeMap::Bind() const
{
	glDepthFunc(GL_LEQUAL);
	_shader->Bind();
	glBindTexture(GL_TEXTURE_CUBE_MAP, _cubemapID);
	glActiveTexture(GL_TEXTURE0);
	glDepthMask(GL_FALSE);
}

void MatCubeMap::UnBind() const
{
	_shader->UnBind();
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	glDepthFunc(GL_LESS);
	glDepthMask(GL_TRUE);
}

void MatCubeMap::UploadData(const glm::mat4& model, const glm::mat4& view, const glm::mat4& projection)
{
	_shader->UploadInt("uSkyBox", 0);
	_shader->UploadMat4("uView", glm::mat4(glm::mat3(view)));
	_shader->UploadMat4("uProjection", projection);
}
