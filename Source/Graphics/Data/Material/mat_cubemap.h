// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "core"
#include "material.h"
#include "Graphics/Shader/shader.h"

class MatCubeMap : public MxnMaterial
{
public:
	MatCubeMap(const fs::path& shaderPath, const std::vector<fs::path>& facePaths);
	~MatCubeMap() override;

	void Bind() const override;
	void UnBind() const override;
	
	void UploadData(const glm::mat4& model, const glm::mat4& view, const glm::mat4& projection) override;

private:
	unsigned int _cubemapID;
};
