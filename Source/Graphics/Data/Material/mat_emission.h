// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "material.h"

class MatEmission : public MxnMaterial
{
public:
	MatEmission(const fs::path& shaderPath) : MxnMaterial(shaderPath) {}
	~MatEmission() override { delete _shader; }
 
	void Bind() const override { _shader->Bind(); }
	void UnBind() const override { _shader->UnBind(); }
	
	void UploadData(const glm::mat4& model, const glm::mat4& view, const glm::mat4& projection) override
	{
		_shader->UploadMat4("uModel", model);
		_shader->UploadMat4("uView", view);
		_shader->UploadMat4("uProjection", projection);
	}
};
