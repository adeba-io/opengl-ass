// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "mat_tex_complete.h"

#include "render"
#include "debug"
#include "NodeTree/light_node.h"

MatTextureComplete::MatTextureComplete(const fs::path& shaderPath, const std::vector<fs::path>& texturePaths, float shininess)
	: _textures(texturePaths.size()), _shininess(shininess), MxnMaterial(shaderPath)
{
	for (unsigned int i = 0; i < texturePaths.size(); i++)
		_textures[i] = new Texture(texturePaths[i]);
}

MatTextureComplete::~MatTextureComplete()
{
	delete _shader;
	for (unsigned int i = 0; i < _textures.size(); i++)
		delete _textures[i];
}

void MatTextureComplete::Bind() const
{
	_shader->Bind();
	for (unsigned int i = 0; i < _textures.size(); i++)
		_textures[i]->Bind(i);
}

void MatTextureComplete::UnBind() const
{
	_shader->UnBind();
	for (unsigned int i = 0; i < _textures.size(); i++)
		_textures[i]->UnBind();
}

void MatTextureComplete::UploadData(const glm::mat4& model, const glm::mat4& view, const glm::mat4& projection)
{
	_shader->UploadMat4("uModel", model);
	_shader->UploadMat4("uView", view);
	_shader->UploadMat4("uProjection", projection);

	std::string texturePaths = "uSurface.texture";
	for (unsigned int i = 0; i < _textures.size(); i++)
		_shader->UploadInt((texturePaths + std::to_string(i + 1)).c_str(), (int)i);
	_shader->UploadFloat("uSurface.shininess", _shininess);

	_shader->UploadFloat3("uCamPos", Renderer::GetCameraPosition());

	int pointLightIndex = 0;
	for (auto light : Renderer::GetLights())
	{
		float intensity = light->on ? light->intensity : 0.0f;
		
		switch (light->lType)
		{
			case LightType::Position:
			{
				std::string name = "uPLights[" + std::to_string(pointLightIndex++) + "].";
				_shader->UploadFloat3((name + std::string("position")).c_str(), light->position);
				_shader->UploadFloat3((name + std::string("ambient")).c_str(), light->ambient * intensity);
				_shader->UploadFloat3((name + std::string("diffuse")).c_str(), light->diffuse* intensity);
				_shader->UploadFloat3((name + std::string("specular")).c_str(), light->specular * intensity);

				_shader->UploadFloat((name + std::string("constant")).c_str(), light->constant);
				_shader->UploadFloat((name + std::string("linear")).c_str(), light->linear);
				_shader->UploadFloat((name + std::string("quadratic")).c_str(), light->quadratic);
				break;
			}
			case LightType::Direction:
			{
				_shader->UploadFloat3("uDLight.direction", light->direction);
				_shader->UploadFloat3("uDLight.ambient", light->ambient * intensity);
				_shader->UploadFloat3("uDLight.diffuse", light->diffuse* intensity);
				_shader->UploadFloat3("uDLight.specular", light->specular * intensity);
				break;
			}
			case LightType::Spot:
			{
				_shader->UploadFloat3("uSLight.position", light->position);
				_shader->UploadFloat3("uSLight.direction", -light->realDirection);

				_shader->UploadFloat3("uSLight.ambient", light->ambient * intensity);
				_shader->UploadFloat3("uSLight.diffuse", light->diffuse* intensity);
				_shader->UploadFloat3("uSLight.specular", light->specular * intensity);

				_shader->UploadFloat("uSLight.cutOff", glm::cos(glm::radians(light->cutOff)));
				_shader->UploadFloat("uSLight.outerCutOff", glm::cos(glm::radians(light->outerCutOff)));

				_shader->UploadFloat("uSLight.constant", light->constant);
				_shader->UploadFloat("uSLight.linear", light->linear);
				_shader->UploadFloat("uSLight.quadratic", light->quadratic);
				break;
			}
		}
	}
}
