// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "core"
#include "material.h"
#include "../texture.h"

class MatTextureComplete : public MxnMaterial
{
public:
	MatTextureComplete(const fs::path& shaderPath, const std::vector<fs::path>& texturePaths, float shininess);
	~MatTextureComplete() override;
 
	void Bind() const override;
	void UnBind() const override;
	
	void UploadData(const glm::mat4& model, const glm::mat4& view, const glm::mat4& projection) override;

private:
	std::vector<Texture*> _textures;

	float _shininess;
};
