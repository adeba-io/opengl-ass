// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "material.h"

class MatTimeTexture : public MxnMaterial
{
public:
	MatTimeTexture(const fs::path& shaderPath, const std::vector<fs::path>& texturePaths, const glm::vec3& specular, float shininess);
	~MatTimeTexture() override;
 
	void Bind() const override;
	void UnBind() const override;
	
	void UploadData(const glm::mat4& model, const glm::mat4& view, const glm::mat4& projection) override;

private:
	std::vector<Texture*> _textures;
	glm::vec3 _specular;
	float _shininess;
};
