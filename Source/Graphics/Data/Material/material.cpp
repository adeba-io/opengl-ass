// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "material.h"

#include "core"
#include "render"
#include "debug"
#include "gameloop"
#include "NodeTree/light_node.h"

#include "Util/strings.h"
#include "mat_complete.h"
#include "mat_tex_complete.h"
#include "mat_time_texture.h"
#include "mat_emission.h"

MxnMaterial::MxnMaterial(const fs::path& shaderPath) : _shader(new Shader(shaderPath)) {}


MatPlain::MatPlain(const fs::path& shaderPath, const std::vector<fs::path>& texturePaths,
			const glm::vec3& diffuse, const glm::vec3& specular, float shininess)
			: _textures(texturePaths.size()),
			  _diffuse(diffuse), _specular(specular), _shininess(shininess), MxnMaterial(shaderPath)
{
	for (unsigned int i = 0; i < texturePaths.size(); i++)
		_textures[i] = new Texture(texturePaths[i]);
}

MatPlain::~MatPlain()
{
	delete _shader;
	for (unsigned int i = 0; i < _textures.size(); i++)
		delete _textures[i];
}

void MatPlain::UploadData(const glm::mat4& model, const glm::mat4& view, const glm::mat4& projection)
{
	_shader->UploadMat4("uModel", model);
	_shader->UploadMat4("uView", view);
	_shader->UploadMat4("uProjection", projection);

	std::string texturePath = "uSurface.texture";
	for (unsigned int i = 0; i < _textures.size(); i++)
		_shader->UploadInt((texturePath + std::to_string(i + 1)).c_str(), i);
	_shader->UploadFloat3("uSurface.diffuse", _diffuse);
	_shader->UploadFloat3("uSurface.specular", _specular);
	_shader->UploadFloat("uSurface.shininess", _shininess);

	_shader->UploadFloat("uTime", Time::GetTime());

	_shader->UploadFloat3("uCamPos", Renderer::GetCameraPosition());

	int pointLightIndex = 0;
	for (auto light : Renderer::GetLights())
	{
		float intensity = light->on ? light->intensity : 0.0f;
		
		switch (light->lType)
		{
			case LightType::Position:
			{
				std::string name = "uPLights[" + std::to_string(pointLightIndex++) + "].";
				_shader->UploadFloat3((name + std::string("position")).c_str(), light->position);
				_shader->UploadFloat3((name + std::string("ambient")).c_str(), light->ambient * intensity);
				_shader->UploadFloat3((name + std::string("diffuse")).c_str(), light->diffuse* intensity);
				_shader->UploadFloat3((name + std::string("specular")).c_str(), light->specular * intensity);

				_shader->UploadFloat((name + std::string("constant")).c_str(), light->constant);
				_shader->UploadFloat((name + std::string("linear")).c_str(), light->linear);
				_shader->UploadFloat((name + std::string("quadratic")).c_str(), light->quadratic);
				break;
			}
			case LightType::Direction:
			{
				_shader->UploadFloat3("uDLight.direction", light->direction);
				_shader->UploadFloat3("uDLight.ambient", light->ambient * intensity);
				_shader->UploadFloat3("uDLight.diffuse", light->diffuse* intensity);
				_shader->UploadFloat3("uDLight.specular", light->specular * intensity);
				break;
			}
			case LightType::Spot:
			{
				// LG_INF("Pos {} {} {}", light->position.x, light->position.y, light->position.z);
				// LG_INF("Dir {} {} {}", light->realDirection.x, light->realDirection.y, light->realDirection.z);
				_shader->UploadFloat3("uSLight.position", light->position);
				_shader->UploadFloat3("uSLight.direction", -light->realDirection);

				_shader->UploadFloat3("uSLight.ambient", light->ambient * intensity);
				_shader->UploadFloat3("uSLight.diffuse", light->diffuse* intensity);
				_shader->UploadFloat3("uSLight.specular", light->specular * intensity);

				_shader->UploadFloat("uSLight.cutOff", glm::cos(glm::radians(light->cutOff)));
				_shader->UploadFloat("uSLight.outerCutOff", glm::cos(glm::radians(light->outerCutOff)));

				_shader->UploadFloat("uSLight.constant", light->constant);
				_shader->UploadFloat("uSLight.linear", light->linear);
				_shader->UploadFloat("uSLight.quadratic", light->quadratic);
				break;
			}
		}
	}
}

void MatPlain::Bind() const
{
	_shader->Bind();
}
void MatPlain::UnBind() const
{
	_shader->UnBind();
}

#define PARAMS const fs::path& shaderPath, const std::vector<fs::path>& texturePaths, const glm::vec3& diffuse, const glm::vec3& specular, float shininess

std::unordered_map<std::string, MxnMaterial* (*)(const fs::path&, const std::vector<fs::path>&, const glm::vec3&, const glm::vec3&, float)>
	_mapNameToMaterial =
{
	{ "complete", [](PARAMS) { return (MxnMaterial*)new MatComplete(shaderPath, diffuse, specular, shininess); } },
	{ "tex_complete", [](PARAMS) { return (MxnMaterial*)new MatTextureComplete(shaderPath, texturePaths, shininess); } },
	{ "phone_screen", [](PARAMS) { return (MxnMaterial*)new MatTimeTexture(shaderPath, texturePaths, specular, shininess); } },
	{ "sky", [](PARAMS) { return (MxnMaterial*)new MatTimeTexture(shaderPath, texturePaths, specular, shininess); } },
	{ "emission", [](PARAMS) { return (MxnMaterial*)new MatEmission(shaderPath); } }
};

MxnMaterial* GetAppropriateMaterial(PARAMS)
{
	std::string fileName = SplitString(shaderPath.string(), '\\').back();
	std::string shaderName = SplitString(fileName, '.')[0];
	
	auto it = _mapNameToMaterial.find(shaderName);
	if (it == _mapNameToMaterial.end())
	{
		LG_WRN("Shader at path {} not found, using default material instead", shaderPath);
		return new MatPlain(shaderPath, texturePaths, diffuse, specular, shininess);
	}

	return it->second(shaderPath, texturePaths, diffuse, specular, shininess);
}
