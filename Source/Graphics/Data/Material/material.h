// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include <glm/glm.hpp>
#include "Graphics/Shader/shader.h"
#include "../texture.h"

class MxnMaterial
{
public:
	virtual ~MxnMaterial() {}

	virtual void Bind() const = 0;
	virtual void UnBind() const = 0;

	const Shader& GetShader() const { return *_shader; }

	virtual void UploadData(const glm::mat4& model, const glm::mat4& view, const glm::mat4& projection) = 0;

protected:
	MxnMaterial(const fs::path& shaderPath);
	Shader *_shader;
};

MxnMaterial* GetAppropriateMaterial(const fs::path& shaderPath, const std::vector<fs::path>& texturePaths, const glm::vec3& diffuse, const glm::vec3& specular, float shininess);

class MatPlain : public MxnMaterial
{
public:
	MatPlain(const fs::path& shaderPath, const std::vector<fs::path>& texturePaths,
			const glm::vec3& diffuse, const glm::vec3& specular, float shininess);
	~MatPlain() override;

	void Bind() const override;
	void UnBind() const override;
	
	void UploadData(const glm::mat4& model, const glm::mat4& view, const glm::mat4& projection) override;

private:
	std::vector<Texture*> _textures;

	glm::vec3 _diffuse;
	glm::vec3 _specular;
	float _shininess;
};
