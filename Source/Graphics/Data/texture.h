// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "core"

class Texture
{
public:
	Texture(const fs::path& texturePath);
	~Texture();

	void Bind(unsigned int index) const;
	void UnBind() const;

private:
	unsigned int _ID;
	int _width;
	int _height;
	int _numColourChannels;
};
