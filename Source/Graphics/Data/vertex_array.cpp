// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "vertex_array.h"
#include <glad/glad.h>
#include "core"

GLenum ShaderDataTypeToGLType(ShaderDataType type)
{
	switch (type)
	{
		case ShaderDataType::Bool:      return GL_BOOL;
		case ShaderDataType::Float1:    return GL_FLOAT;
		case ShaderDataType::Float2:    return GL_FLOAT;
		case ShaderDataType::Float3:    return GL_FLOAT;
		case ShaderDataType::Float4:    return GL_FLOAT;
		case ShaderDataType::Int1:      return GL_INT;
		case ShaderDataType::Int2:      return GL_INT;
		case ShaderDataType::Int3:      return GL_INT;
		case ShaderDataType::Int4:      return GL_INT;
		case ShaderDataType::Mat3x3:    return GL_FLOAT;
		case ShaderDataType::Mat4x4:    return GL_FLOAT;
	}

	ASSERT(false, "Unknown ShaderDataType");
	return 0;
}

VertexArray::VertexArray()
{
	glCreateVertexArrays(1, &_ID);
}

VertexArray::~VertexArray()
{
	glDeleteVertexArrays(1, &_ID);
}

void VertexArray::Bind() const { glBindVertexArray(_ID); }

void VertexArray::UnBind() const { glBindVertexArray(0); }

void VertexArray::AddVertexBuffer(const VertexBuffer* vertexBuffer)
{
	_vertexBuffers.push_back(vertexBuffer);

	glBindVertexArray(_ID);
	vertexBuffer->Bind();
	
	unsigned int index = 0;
	unsigned int stride = vertexBuffer->GetLayout().GetStride();
	for (const auto& element : vertexBuffer->GetLayout())
	{
		glEnableVertexAttribArray(index);
		glVertexAttribPointer(
			index, 
			element.GetComponentCount(), 
			ShaderDataTypeToGLType(element.type), 
			element.normalised ? GL_TRUE : GL_FALSE, 
			stride, 
			(const void*)(long long)element.offset
		);
		index++;
	}
}
void VertexArray::SetIndexBuffer(IndexBuffer *indexBuffer)
{
	glBindVertexArray(_ID);
	indexBuffer->Bind();
	_indexBuffer = indexBuffer;
}
