// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once
#include "core"
#include "buffers.h"

class VertexArray
{
public:
	VertexArray();
	~VertexArray();
	
	void Bind() const;
	void UnBind() const;

	inline unsigned int GetID() const { return _ID; }
	
	void AddVertexBuffer(const VertexBuffer *vertexBuffer);
	inline const std::vector<const VertexBuffer*>& GetVertexBuffers() const { return _vertexBuffers; }

	void SetIndexBuffer(IndexBuffer *indexBuffer);
	inline const IndexBuffer* GetIndexBuffer() const { return _indexBuffer; }

private:
	unsigned int _ID;
	std::vector<const VertexBuffer*> _vertexBuffers;
	IndexBuffer *_indexBuffer;
};
