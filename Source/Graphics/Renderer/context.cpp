// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "context.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "debug"

RenderContext::RenderContext(GLFWwindow *windowHandle) : _windowHandle(windowHandle)
{
	ASSERT(windowHandle, "Window Handle is null!");
}

void RenderContext::Init()
{
	glfwMakeContextCurrent(_windowHandle);
	int status = gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
	ASSERT(status, "Failed to initialise GLAD");

	LG_INF("OpenGL Renderer: {} {} {}", glGetString(GL_VENDOR), glGetString(GL_RENDERER), glGetString(GL_VERSION));
}

void RenderContext::SwapBuffers()
{
	glfwSwapBuffers(_windowHandle);
}
