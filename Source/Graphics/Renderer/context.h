// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include <GLFW/glfw3.h>

class RenderContext
{
public:
	RenderContext(GLFWwindow *window);

	void Init();
	void SwapBuffers();

private:
	GLFWwindow *_windowHandle;
};
