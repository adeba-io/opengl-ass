// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "render_api.h"
#include <glad/glad.h>
#include "debug"

void RenderAPI::SetClearColour(const glm::vec4& colour)
{
	glClearColor(colour.r, colour.g, colour.b, colour.a);
}

void RenderAPI::Clear()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void RenderAPI::DrawIndexed(const VertexArray& vertexArray)
{
	vertexArray.Bind();
	glDrawElements(GL_TRIANGLES, vertexArray.GetIndexBuffer()->GetCount(), GL_UNSIGNED_INT, nullptr);
}

#ifdef BLD_DEBUG
void RenderAPI::CheckForErrors()
{
	while (GLenum error = glGetError())
	{
		LG_TRC("[OpenGL Error] ({})", error);
	}
}
#endif
