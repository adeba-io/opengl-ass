// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "math"
#include "core"
#include "debug"
#include "util/singleton"

#include "Graphics/Data/vertex_array.h"

class RenderAPI
{
public:
	static void SetClearColour(const glm::vec4& colour);
	static void Clear();

	static void DrawIndexed(const VertexArray& vertexArray);

#ifdef BLD_DEBUG
	static void CheckForErrors();
#endif
};
