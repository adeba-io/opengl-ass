// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "renderer.h"
#include <glm/gtc/matrix_transform.hpp>
#include "debug"

#include "Graphics/Shader/shader.h"
#include "NodeTree/node.h"

Renderer::Renderer()
	: _view(1.f), _projection(1.f), _ambient(), MxnSingleton<Renderer>(this)
{}

glm::mat4 CalculateNodeMatrix(const MxnNode *node);

void Renderer::BeginScene(const NdCamera& camera)
{
	glm::mat4 view = CalculateNodeMatrix(&camera);
	Get()->_camPos = -glm::vec3(view[3].x, view[3].y, view[3].z);
	Get()->_view = view;
	Get()->_projection = camera.GetProjection();

	std::vector<NdLight*>& lights = Get()->_lights;
	for (unsigned int i = 0; i < lights.size(); i++)
	{
		glm::mat4 m = CalculateNodeMatrix(lights[i]);
		lights[i]->position = glm::vec3(-m[3].x, m[3].y, -m[3].z);

		glm::vec3 dir = glm::vec3(m[2]) * lights[i]->direction;

		lights[i]->realDirection = glm::normalize(glm::vec3(dir.x, dir.y, dir.z));
	}
}

void Renderer::EndScene() {}

void Renderer::Submit(NdMesh *meshNode)
{
	glm::mat4 model = glm::inverse(CalculateNodeMatrix(meshNode));

	meshNode->GetMaterial().Bind();
	meshNode->GetMaterial().UploadData(model, Get()->_view, Get()->_projection);
	RenderAPI::CheckForErrors();
	RenderAPI::DrawIndexed(meshNode->GetVertexArray());
	meshNode->GetMaterial().UnBind();
}

void Renderer::AddLight(NdLight *light)
{
	std::vector<NdLight*>& lights = Get()->_lights;
	auto it = std::find(lights.begin(), lights.end(), light);
	if (it != lights.end())
	{
		LG_WRN("Tried to add light {} but it is already registered", light->GetName());
		return;
	}

	lights.push_back(light);
}

void Renderer::RemoveLight(NdLight *light)
{
	std::vector<NdLight*>& lights = Get()->_lights;
	auto it = std::find(lights.begin(), lights.end(), light);
	if (it == lights.end())
	{
		LG_WRN("Tried to remove light {} but it is not registered", light->GetName());
		return;
	}

	lights.erase(it);
}

glm::mat4 CalculateNodeMatrix(const MxnNode *node)
{
	glm::mat4 mat(1.0f);

	node = node->GetParent();
	while (node != nullptr)
	{
		mat *= node->GetMatrix();
		node = node->GetParent();
	}

	return mat;
}
