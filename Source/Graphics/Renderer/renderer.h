// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include <glad/glad.h>
#include "nodetree"
#include "util/singleton"

#include "render_api.h"
#include "Graphics/Shader/shader.h"
#include "NodeTree/camera_node.h"
#include "NodeTree/mesh_node.h"
#include "NodeTree/light_node.h"

class Renderer : public MxnSingleton<Renderer>
{
public:
	static void Init() { new Renderer; }

	static void BeginScene(const NdCamera& camera);
	static void EndScene();

	static void Submit(NdMesh *meshNode);

	static void AddLight(NdLight *light);
	static void RemoveLight(NdLight *light);

	static void SetAmbientColour(const glm::vec3& ambient) { Get()->_ambient = glm::vec3(ambient); }

	static const glm::vec3& GetCameraPosition() { return Get()->_camPos; }

	static const std::vector<NdLight*>& GetLights() { return Get()->_lights; }

private:
	Renderer();

	glm::mat4 _view;
	glm::mat4 _projection;
	glm::vec3 _camPos;

	glm::vec3 _ambient;
	std::vector<NdLight*> _lights;
};

