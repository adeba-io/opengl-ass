// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "shader.h"

#include "math"
#include <glm/gtc/type_ptr.hpp>
#include <glad/glad.h>
#include "core"
#include "debug"
#include "render"

#include "shader_compilation.h"

Shader::Shader(const fs::path& filepath) : _ID(0), _filepath(filepath), _uniformMap{}
{
	if (filepath.empty()) return;
	SrcShader src = ParseShader(filepath);
	CreateShader(src.vertex, src.pixel);
}

Shader::Shader(const std::string& srcVertex, const std::string& srcPixel) : _ID(0), _filepath(), _uniformMap{}
{
	CreateShader(srcVertex, srcPixel);
}

Shader::~Shader()
{
	glDeleteProgram(_ID);
}

void Shader::Bind() const
{
	glUseProgram(_ID);
}

void Shader::UnBind() const
{
	glUseProgram(0);
}

void Shader::UploadInt(const char *uniform, int value)
{
	glUniform1i(GetUniformLocation(uniform), value);
}

void Shader::UploadFloat(const char *uniform, float value)
{
	glUniform1fv(GetUniformLocation(uniform), 1, &value);
}

void Shader::UploadFloat2(const char *uniform, const glm::vec2& value)
{
	glUniform2fv(GetUniformLocation(uniform), 1, glm::value_ptr(value));
}

void Shader::UploadFloat3(const char *uniform, const glm::vec3& value)
{
	glUniform3fv(GetUniformLocation(uniform), 1, glm::value_ptr(value));
}

void Shader::UploadFloat4(const char *uniform, const glm::vec4& value)
{
	glUniform4fv(GetUniformLocation(uniform), 1, glm::value_ptr(value));
}

void Shader::UploadMat4(const char *uniform, const glm::mat4& matrix)
{
	glUniformMatrix4fv(GetUniformLocation(uniform), 1, GL_FALSE, glm::value_ptr(matrix));
}

void Shader::UploadMat3(const char *uniform, const glm::mat3& matrix)
{
	glUniformMatrix3fv(GetUniformLocation(uniform), 1, GL_FALSE, glm::value_ptr(matrix));
}

void Shader::CreateShader(const std::string& srcVertex, const std::string& srcPixel)
{
	unsigned int idVertex = CompileShader(TyShader::VERTEX, srcVertex);
	unsigned int idPixel = CompileShader(TyShader::PIXEL, srcPixel);
	_ID = CreateProgram(idVertex, idPixel);
}

int Shader::GetUniformLocation(const char *uniform)
{
	if (_uniformMap.find(uniform) != _uniformMap.end())
		return _uniformMap[uniform];
	
	int id = glGetUniformLocation(_ID, uniform);
	_uniformMap[uniform] = id;

	if (id == -1)
		LG_ERR("Uniform {} does not exist in shader {}", uniform, _filepath);
	
	return id;
}
