// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "math"
#include "core"

class Shader
{
public:
	Shader(const fs::path& filepath);
	Shader(const std::string& srcVertex, const std::string& srcPixel);
	~Shader();

	void Bind() const;
	void UnBind() const;

	void UploadInt(const char *uniform, int value);

	void UploadFloat(const char *uniform, float value);
	void UploadFloat2(const char *uniform, const glm::vec2& value);
	void UploadFloat3(const char *uniform, const glm::vec3& value);
	void UploadFloat4(const char *uniform, const glm::vec4& value);

	void UploadMat4(const char *uniform, const glm::mat4& matrix);
	void UploadMat3(const char *uniform, const glm::mat3& matrix);

private:
	void CreateShader(const std::string& srcVertex, const std::string& srcPixel);
	int GetUniformLocation(const char *uniform);

	unsigned int _ID;
	fs::path _filepath;
	std::unordered_map<std::string, int> _uniformMap;
};
