// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "shader_compilation.h"

#include <glad/glad.h>
#include "core"
#include "debug"

SrcShader ParseShader(const fs::path& shaderPath)
{
	std::ifstream src(shaderPath.string());

	std::string line;
	std::stringstream ss[2];
	TyShader type = TyShader::NONE;

	while(getline(src, line))
	{
		if (line.find("#shader") != std::string::npos)
		{
			if (line.find("vertex") != std::string::npos)
				type = TyShader::VERTEX;
			else if (line.find("pixel") != std::string::npos)
				type = TyShader::PIXEL;
			
			continue;
		}

		if (type != TyShader::NONE)
			ss[(int) type] << line << "\n";
	}

	return { ss[0].str(), ss[1].str() };
}

unsigned int CompileShader(TyShader type, const std::string& source)
{
	unsigned int glType = 0;
	const char* shaderName;
	switch (type)
	{
		case TyShader::VERTEX:
			glType = GL_VERTEX_SHADER;
			shaderName = "Vertex";
			break;
		case TyShader::PIXEL:
			glType = GL_FRAGMENT_SHADER;
			shaderName = "Pixel";
			break;
		default:
			LG_ERR("Shader type not accounted for {}", (int)type);
			return 0;
	}

	// Send shader source code to GL
	unsigned int shader = glCreateShader(glType);
	const char *cStrSource = (const char *)source.c_str();
	glShaderSource(shader, 1, &cStrSource, 0);

	glCompileShader(shader);

	GLint isCompiled = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);

	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> infoLog(maxLength);
		glGetShaderInfoLog(shader, maxLength, &maxLength, &infoLog[0]);
		
		// We don't need the shader anymore.
		glDeleteShader(shader);

		// Use the infoLog as you see fit.
		LG_ERR("{} Shader compilation failure: {}", shaderName, &infoLog[0]);
		return 0;
	}

	return shader;
}


unsigned int CreateProgram(unsigned int idVertex, unsigned int idPixel)
{
	// Vertex and fragment shaders are successfully compiled.
	// Now time to link them together into a program.
	// Get a program object.
	GLuint program = glCreateProgram();

	// Attach our shaders to our program
	glAttachShader(program, idVertex);
	glAttachShader(program, idPixel);

	// Link our program
	glLinkProgram(program);

	// Note the different functions here: glGetProgram* instead of glGetShader*.
	GLint isLinked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);
		
		// We don't need the program anymore.
		glDeleteProgram(program);
		// Don't leak shaders either.
		glDeleteShader(idVertex);
		glDeleteShader(idPixel);

		// Use the infoLog as you see fit.
		LG_ERR("Shader linking failure: {}", &infoLog[0]);
		
		// In this simple program, we'll just leave
		return 0;
	}

	// Always detach shaders after a successful link.
	glDetachShader(program, idVertex);
	glDetachShader(program, idPixel);
	return program;
}
