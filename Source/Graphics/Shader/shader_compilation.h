// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once
#include "core"

struct SrcShader
{
	std::string vertex;
	std::string pixel;
};

enum class TyShader { NONE = -1, VERTEX = 0, PIXEL = 1 };

SrcShader ParseShader(const fs::path& shaderPath);
unsigned int CompileShader(TyShader type, const std::string& source);
unsigned int CreateProgram(unsigned int idVertex, unsigned int idPixel);
