// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "enm_input.h"

void ToNextState(EInputState& state, bool isPressed)
{
	switch (state)
	{
		case EInputState::RELEASED:
			state = isPressed ? EInputState::DOWN : state;
			break;
		case EInputState::DOWN:
			state = isPressed ? EInputState::HELD	: EInputState::UP;
			break;
		case EInputState::HELD:
			state = isPressed ? EInputState::HELD : EInputState::UP;
			break;
		case EInputState::UP:
			state = isPressed ? EInputState::DOWN : EInputState::RELEASED;
			break;
	}
}

// bool IsPressed(const EInputState& state) { return state == EInputState::DOWN || state == EInputState::HELD; }

std::ostream& operator<<(std::ostream& out, const EInputState& state) noexcept
{
	switch (state)
	{
		case EInputState::RELEASED:
			out << "Released";
			break;
		case EInputState::DOWN:
			out << "Down";
			break;
		case EInputState::HELD:
			out << "Held";
			break;
		case EInputState::UP:
			out << "Up";
			break;
	}
	return out;
}
