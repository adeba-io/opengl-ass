// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once
#include "core"

enum class EInputState { RELEASED = 0, DOWN = 1, HELD = 2, UP = 3 };

void ToNextState(EInputState& state, bool isPressed);

std::ostream& operator<<(std::ostream& out, const EInputState& state) noexcept;

inline bool IsDown(const EInputState& state) { return state == EInputState::DOWN; }
inline bool IsPressed(const EInputState& state)  { return state == EInputState::DOWN || state == EInputState::HELD; }
