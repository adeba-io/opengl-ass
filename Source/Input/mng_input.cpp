// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include <GLFW/glfw3.h>
#include "app"
#include "core"
#include "input"
#include "events"

static std::vector<unsigned int> SKeys = {
	// GLFW_KEY_SPACE, GLFW_KEY_APOSTROPHE, GLFW_KEY_COMMA, GLFW_KEY_MINUS, GLFW_KEY_PERIOD, GLFW_KEY_FSLASH, GLFW_KEY_SEMICOLON,
	// GLFW_KEY_EQUAL, GLFW_KEY_LBRACKET, GLFW_KEY_BSLASH, GLFW_KEY_RBRACKET, GLFW_KEY_GRAVE,
	GLFW_KEY_SPACE,

	GLFW_KEY_0, GLFW_KEY_1, GLFW_KEY_2, GLFW_KEY_3, GLFW_KEY_4, GLFW_KEY_5, GLFW_KEY_6, GLFW_KEY_7, GLFW_KEY_8, GLFW_KEY_9,

	GLFW_KEY_A, GLFW_KEY_B, GLFW_KEY_C, GLFW_KEY_D, GLFW_KEY_E, GLFW_KEY_F, GLFW_KEY_G, GLFW_KEY_H, GLFW_KEY_I, GLFW_KEY_J,
	GLFW_KEY_K, GLFW_KEY_L, GLFW_KEY_M, GLFW_KEY_N, GLFW_KEY_O, GLFW_KEY_P, GLFW_KEY_Q, GLFW_KEY_R, GLFW_KEY_S, GLFW_KEY_T,
	GLFW_KEY_U, GLFW_KEY_V, GLFW_KEY_W, GLFW_KEY_X, GLFW_KEY_Y, GLFW_KEY_Z,

	// Function Keys
	GLFW_KEY_ESCAPE, GLFW_KEY_ENTER, GLFW_KEY_TAB, GLFW_KEY_BACKSPACE, GLFW_KEY_INSERT, GLFW_KEY_DELETE,
	GLFW_KEY_RIGHT, GLFW_KEY_LEFT, GLFW_KEY_DOWN, GLFW_KEY_UP, GLFW_KEY_PAGE_UP, GLFW_KEY_PAGE_DOWN, GLFW_KEY_HOME, GLFW_KEY_END,
	GLFW_KEY_NUM_LOCK, GLFW_KEY_PRINT_SCREEN, GLFW_KEY_PAUSE,
	GLFW_KEY_F1, GLFW_KEY_F2, GLFW_KEY_F3, GLFW_KEY_F4, GLFW_KEY_F5, GLFW_KEY_F6, GLFW_KEY_F7, GLFW_KEY_F8, GLFW_KEY_F9, GLFW_KEY_F10, GLFW_KEY_F11, GLFW_KEY_F12, GLFW_KEY_F13,
	GLFW_KEY_F14, GLFW_KEY_F15, GLFW_KEY_F16, GLFW_KEY_F17, GLFW_KEY_F18, GLFW_KEY_F19, GLFW_KEY_F20, GLFW_KEY_F21, GLFW_KEY_F22, GLFW_KEY_F23, GLFW_KEY_F24, GLFW_KEY_F25,

	GLFW_KEY_KP_0, GLFW_KEY_KP_1, GLFW_KEY_KP_2, GLFW_KEY_KP_3, GLFW_KEY_KP_4, GLFW_KEY_KP_5, GLFW_KEY_KP_6, GLFW_KEY_KP_7, GLFW_KEY_KP_8, GLFW_KEY_KP_9,

	GLFW_KEY_KP_DECIMAL, GLFW_KEY_KP_DIVIDE, GLFW_KEY_KP_MULTIPLY, GLFW_KEY_KP_SUBTRACT, GLFW_KEY_KP_ADD, GLFW_KEY_KP_ENTER,GLFW_KEY_KP_EQUAL,

	// GLFW_KEY_LSHIFT, GLFW_KEY_LCONTROL, GLFW_KEY_L_ALT, GLFW_KEY_LSUPER,
	// GLFW_KEY_RSHIFT, GLFW_KEY_RCONTROL, GLFW_KEY_R_ALT, GLFW_KEY_RSUPER,
	// GLFW_KEY_MENU
};

static std::vector<unsigned int> SMouseButtons = {
	GLFW_MOUSE_BUTTON_1, GLFW_MOUSE_BUTTON_2, GLFW_MOUSE_BUTTON_3, GLFW_MOUSE_BUTTON_4,
	GLFW_MOUSE_BUTTON_5, GLFW_MOUSE_BUTTON_6, GLFW_MOUSE_BUTTON_7, GLFW_MOUSE_BUTTON_8
};

// Members
MngInput::MngInput() :
	_mousePos(0.0f, 0.0f), _keyStates(40), _mouseButtonStates(8), MxnSingleton<MngInput>(this)
{
	for (auto it = SKeys.begin(); it != SKeys.end(); ++it)
	{
		_keyStates.emplace(*it, EInputState::RELEASED);
	}

	for (auto it = SMouseButtons.begin(); it != SMouseButtons.end(); ++it)
	{
		_mouseButtonStates.emplace(*it, EInputState::RELEASED);
	}
}

void MngInput::UpdateImpl()
{
	GLFWwindow *window = static_cast<GLFWwindow*>(App::GetWindow().GetNativeWindow());
	for (auto it = _keyStates.begin(); it != _keyStates.end(); ++it)
	{
		auto state = glfwGetKey(window, it->first);
		ToNextState(it->second, state == GLFW_PRESS || state == GLFW_REPEAT);
	}

	for (auto it = _mouseButtonStates.begin(); it != _mouseButtonStates.end(); ++it)
	{
		auto state = glfwGetMouseButton(window, it->first);
		ToNextState(it->second, state == GLFW_PRESS || state == GLFW_REPEAT);
	}
	
	_prevMousePos = _mousePos;

	double x = 0, y = 0;
	glfwGetCursorPos((GLFWwindow*)App::GetWindow().GetNativeWindow(), &x, &y);
	_mousePos.x = (float)x;
	_mousePos.y = (float)y;

	_deltaMousePos = _mousePos - _prevMousePos;
}

EInputState MngInput::GetKeyStateImpl(unsigned int keycode)
{
#ifdef BLD_DEBUG
	if (_keyStates.find(keycode) == _keyStates.end())
	{
		LG_ERR("Key {} is not in the _keyStates map", keycode);
		return EInputState::RELEASED;
	}
#endif
	return _keyStates[keycode];
}

EInputState MngInput::GetMouseButtonStateImpl(unsigned int mouseButton)
{
#ifdef BLD_DEBUG
	if (_mouseButtonStates.find(mouseButton) == _mouseButtonStates.end())
	{
		LG_ERR("Mouse Button {} is not in the _mouseButtonStates map", mouseButton);
		return EInputState::RELEASED;
	}
#endif
	return _mouseButtonStates[mouseButton];
}
