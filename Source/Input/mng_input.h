// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "math"
#include "core"

#include "util/singleton"

#include "enm_input.h"


class MngInput : public MxnSingleton<MngInput>
{
public:
	static void Init() { new MngInput; }

	static inline void Update() { return Get()->UpdateImpl(); }
	static inline EInputState GetKeyState(unsigned int keycode) { return Get()->GetKeyStateImpl(keycode); }
	static inline EInputState GetMouseButtonState(unsigned int mouseButton) { return Get()->GetMouseButtonStateImpl(mouseButton); }
	// static inline vector2 GetMousePosition() { return Get()->GetMousePositionImpl(); }
	static inline const glm::vec2& GetMousePosition() { return Get()->_mousePos; }
	static inline const glm::vec2& GetMouseMovement() { return Get()->_deltaMousePos; }

private:
	MngInput();

	void UpdateImpl();
	EInputState GetKeyStateImpl(unsigned int keycode);
	EInputState GetMouseButtonStateImpl(unsigned int mouseButton);
	// inline vector2 GetMousePositionImpl() const;

	glm::vec2 _mousePos;
	glm::vec2 _prevMousePos;
	glm::vec2 _deltaMousePos;

	std::unordered_map<unsigned int, EInputState> _keyStates;
	std::unordered_map<unsigned char, EInputState> _mouseButtonStates;
};
