// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include <glad/glad.h>
#include <glm/gtc/matrix_transform.hpp>
#include "core"
#include "math"

#include "node.h"

class NdCamera : public MxnNode
{
public:
	NdCamera(const char *name, float fov, float aspect, float near, float far,const fs::path& animPath, const std::vector<MxnNode*>& children = {}, const std::vector<MxnScript*>& scripts = {})
	: _near(near), _far(far), _projection(glm::perspective(glm::radians(fov), aspect, near, far)), MxnNode(name, animPath, children, scripts) {}

	inline const glm::mat4& GetProjection() const { return _projection; }

	std::string str() const override
	{
		std::stringstream ss;
		ss << "Camera " << _name << " Near-Far: " << _near << "-" << _far;
		return ss.str();
	}

private:
	float _near;
	float _far;
	glm::mat4 _projection;
};
