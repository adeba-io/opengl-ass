// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "light_node.h"
#include "render"
#include "debug"

NdLight::NdLight(const char *name, LightType type, bool on, const glm::vec3& direction, const glm::vec3& ambient, const glm::vec3& diffuse, const glm::vec3& specular,
		float intensity, float constant, float linear, float quadratic,
		float cutOff, float outerCutOff,const fs::path& animPath, const std::vector<MxnNode*>& children, const std::vector<MxnScript*>& scripts)
	: on(on), lType(type), direction(direction), realDirection(direction), ambient(ambient), diffuse(diffuse), specular(specular),
		intensity(intensity), constant(constant), linear(linear), quadratic(quadratic),
		cutOff(cutOff), outerCutOff(outerCutOff), MxnNode(name, animPath, children, scripts)
{
	Renderer::AddLight(this);
}

NdLight::~NdLight()
{
	Renderer::RemoveLight(this);
}
std::string NdLight::str() const
{
	std::stringstream ss;
	ss << (lType == LightType::Direction ? "Direction" : (lType == LightType::Spot ? "Spot" : "Position")) << " Light " << _name;
	return ss.str();
}

LightType LTypeFromString(const std::string& str)
{
	if (str == "direction")
		return LightType::Direction;
	if (str == "spot")
		return LightType::Spot;
	
	return LightType::Position;
}
