// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "node.h"

enum class LightType { Position, Direction, Spot };

class NdLight : public MxnNode
{
public:
	NdLight(const char *name, LightType type, bool on, const glm::vec3& direction,
		const glm::vec3& ambient, const glm::vec3& diffuse, const glm::vec3& specular, float intensity,
		float constant, float linear, float quadratic,
		float cutOff, float outerCutOff,
		const fs::path& animPath, const std::vector<MxnNode*>& children = {}, const std::vector<MxnScript*>& scripts = {});
	~NdLight();

	std::string str() const override;

	bool on = true;

	LightType lType;
	glm::vec3 position;
	glm::vec3 direction;
	glm::vec3 realDirection;

	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
	float intensity;
	
	float constant;
	float linear;
	float quadratic;
	float cutOff, outerCutOff;
};

LightType LTypeFromString(const std::string& str);
