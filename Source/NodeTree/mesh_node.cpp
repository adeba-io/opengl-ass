// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "mesh_node.h"

#include "render"
#include "debug"
#include "Assets/model_reader.h"

NdMesh::NdMesh(const char *name, const fs::path& meshPath, MxnMaterial *material,const fs::path& animPath, const std::vector<MxnNode*>& children /* = {} */, const std::vector<MxnScript*>& scripts /* = {} */)
	: _vertexArr(nullptr), _vertexBfr(nullptr), _indexBfr(nullptr), _material(material),
		MxnNode(name, animPath, children, scripts)
{
	ParseModel(meshPath, &_vertexArr, &_vertexBfr, &_indexBfr);
}

NdMesh::~NdMesh()
{
	delete _vertexArr;
	delete _vertexBfr;
	delete _indexBfr;
	delete _material;
}

void NdMesh::Process()
{
	MxnNode::Process();;
	Renderer::Submit(this);
}


std::string NdMesh::str() const
{
	std::stringstream ss;
	ss << "Mesh " << _name;
	return ss.str();
}
