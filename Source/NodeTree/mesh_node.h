// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "core"

#include "node.h"
#include "Graphics/Data/vertex_array.h"
#include "Graphics/Data/buffers.h"
#include "Graphics/Data/Material/material.h"
// #include "Graphics/Data/texture.h"
// #include "Graphics/Shader/shader.h"

class NdMesh : public MxnNode
{
public:
	NdMesh(const char *name, const fs::path& meshPath, MxnMaterial *material,const fs::path& animPath, const std::vector<MxnNode*>& children = {}, const std::vector<MxnScript*>& scripts = {});
	~NdMesh();

	void Process() override;

	inline const VertexArray& GetVertexArray() { return *_vertexArr; }
	inline MxnMaterial& GetMaterial() const { return *_material; }

	std::string str() const override;

private:
	VertexArray *_vertexArr;
	VertexBuffer *_vertexBfr;
	IndexBuffer *_indexBfr;
	MxnMaterial *_material;
};
