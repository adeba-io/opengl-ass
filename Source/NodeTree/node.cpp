// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "node.h"
#include "debug"
#include "scripts"

MxnNode::MxnNode(const char *name,const fs::path& animPath, const std::vector<MxnNode*>& children, const std::vector<MxnScript*>& scripts, TransformationType type)
		: _name(name),_animationFile(animPath), _parent(nullptr), _children(children), _scripts(scripts), _transformationType(type)
{
	for (MxnNode* child : children)
		child->SetParent(this);
		
	for (auto it : _scripts)
		it->Begin(this);
}

void MxnNode::AddChild(MxnNode *child)
{
	if (child == _parent)
	{
		LG_WRN("Attempted to add child {} that is {}'s parent", child->GetName(), _name);
		return;
	}

	auto it = std::find(_children.begin(), _children.end(), child);
	if (it != _children.end())
	{
		LG_WRN("Attempted to add child {} that is already a child of {}", child->GetName(), _name);
		return;
	}

	child->SetParent(this);
	_children.push_back(child);
}

void MxnNode::RemoveChild(MxnNode *child)
{
	auto it = std::find(_children.begin(), _children.end(), child);
	if (it == _children.end())
	{
		LG_WRN("Attempted to remove {} that is not a child of  {}", child->GetName(), _name);
		return;
	}

	(*it)->DeParent();
	_children.erase(it);
}

void MxnNode::SetParent(MxnNode *parent)
{
	auto it = std::find(_children.begin(), _children.end(), _parent);
	if (it != _children.end())
	{
		LG_WRN("Attempted to set {} as parent but it is a child of {}", parent->GetName(), _name);
		return;
	}

	_parent = parent;
}

void MxnNode::AddScript(MxnScript *script)
{
	auto it = std::find(_scripts.begin(), _scripts.end(), script);
	if (it != _scripts.end())
	{
		LG_WRN("Tried to add a script that {} already has", _name);
		return;
	}

	_scripts.push_back(script);
}

void MxnNode::RemoveScript(MxnScript *script)
{
	auto it = std::find(_scripts.begin(), _scripts.end(), script);
	if (it == _scripts.end())
	{
		LG_WRN("Tried to remove script that is not in {}", _name);
		return;
	}

	_scripts.erase(it);
}

void MxnNode::Process()
{
	for (auto it : _scripts)
		it->Process();
}

void MxnNode::ImGuiProcess()
{
	for (auto it : _scripts)
		it->ImGuiProcess();
}

std::string NdPlain::str() const
{
	std::stringstream ss;
	ss << "Plain " << _name;
	return ss.str();
}
