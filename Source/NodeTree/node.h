// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include <glm/glm.hpp>
#include "core"

// If I include the MxnScript header file it creates a cyclical reference
// so I'll just forward declare MxnScript
class MxnScript;

enum class TransformationType { None, Translate, Rotate, Scale };

class MxnNode
{
public:
	virtual ~MxnNode() {}
	const std::string& GetName() const { return _name; }
	const fs::path& GetAnimPath() const { return _animationFile; }

	virtual glm::mat4 GetMatrix() const { return glm::mat4(1.f); }
	inline TransformationType GetTransformationType() const { return _transformationType; }
	
	void SetParent(MxnNode *parent);
	inline void DeParent() { _parent = nullptr; }

	void AddChild(MxnNode *child);
	void RemoveChild(MxnNode *child);
	inline const std::vector<MxnNode*>& GetChildren() const { return _children; }

	void AddScript(MxnScript *script);
	void RemoveScript(MxnScript *script);

	std::vector<MxnNode*>::iterator beginChildren() { return _children.begin(); }
	std::vector<MxnNode*>::iterator endChildren() { return _children.end(); }

	inline const MxnNode * GetParent() const { return _parent; }

	virtual void Process();
	virtual void ImGuiProcess();

	virtual std::string str() const = 0;

protected:
	MxnNode(const char *name, const fs::path& animPath, const std::vector<MxnNode*>& children, const std::vector<MxnScript*>& scripts, TransformationType type = TransformationType::None);

	std::string _name;
	fs::path _animationFile;
	MxnNode *_parent;
	std::vector<MxnNode*> _children;
	std::vector<MxnScript*> _scripts;
	TransformationType _transformationType;
};

class MxnVector
{
public:
	MxnVector(glm::vec3 vector) : vector(vector) {}
	glm::vec3 vector;
};

class NdPlain : public MxnNode
{
public:
	NdPlain(const char *name = "Plain", const fs::path& animPath = fs::path(), const std::vector<MxnNode*>& children = {}, const std::vector<MxnScript*>& scripts = {})
		: MxnNode(name, animPath, children, scripts) {}

	std::string str() const override;
};
