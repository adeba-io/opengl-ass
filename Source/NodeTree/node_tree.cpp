// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "node_tree.h"
#include "debug"
// #include "render"

void TraverseNodeTree(MxnNode& root)
{
	root.Process();
	for (auto it = root.beginChildren(); it != root.endChildren(); it++)
		TraverseNodeTree(**it);
}

void RecPrintTree(MxnNode& root, std::stringstream& ss, int depth)
{
	if (&root == nullptr)
		return;
	
	for (int i = 0; i < depth; i++)
		ss << '\t';
	
	ss << root.str() << ":\n";
	depth++;
	for (auto it : root.GetChildren())
		RecPrintTree((*it), ss, depth);
}

std::string PrintTree(MxnNode& root)
{
	std::stringstream ss;
	RecPrintTree(root, ss, 0);
	return ss.str();
}

MxnNode* FindByNameInTree(MxnNode& root, const std::string& name)
{
	if (root.GetName() == name)
		return &root;
	
	for (auto child : root.GetChildren())
	{
		MxnNode *node = FindByNameInTree(*child, name);
		if (node != nullptr)
			return node;
	}

	return nullptr;
}

void DeleteTree(MxnNode *root)
{
	if (root == nullptr) return;

	for (auto child : root->GetChildren())
		DeleteTree(child);
	
	delete root;
}
