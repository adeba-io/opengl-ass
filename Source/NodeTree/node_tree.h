// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "core"
#include "node.h"

void TraverseNodeTree(MxnNode& root);

std::string PrintTree(MxnNode& root);

MxnNode* FindByNameInTree(MxnNode& root, const std::string& name);

void DeleteTree(MxnNode *root);
