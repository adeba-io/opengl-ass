// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "rotate_node.h"
#include <glm/gtc/matrix_transform.hpp>
#include "debug"

NdRotate::NdRotate(const char *name, glm::vec3 rotation, RotationOrder order,const fs::path& animPath, const std::vector<MxnNode*>& children, const std::vector<MxnScript*>& scripts /*= {} */)
	: MxnNode(name, animPath, children, scripts, TransformationType::Rotate), MxnVector(rotation)
{
	switch(order)
	{
		case RotationOrder::XYZ:
		{
			_fnComputeMatrix = [](const glm::vec3& rotation) -> glm::mat4
			{
				return glm::rotate(
					glm::rotate(
						glm::rotate(glm::mat4(1.0f), 
						glm::radians(rotation.x), glm::vec3(1, 0, 0)),
						glm::radians(rotation.y), glm::vec3(0, 1, 0)),
						glm::radians(rotation.z), glm::vec3(0, 0, 1));
			};
			break;
		}
		case RotationOrder::ZXY:
			_fnComputeMatrix = [](const glm::vec3& rotation) -> glm::mat4
			{
				return glm::rotate(
					glm::rotate(
						glm::rotate(glm::mat4(1.f), 
						
						glm::radians(rotation.z), glm::vec3(0, 0, 1)),
						glm::radians(rotation.x), glm::vec3(1, 0, 0)),
						glm::radians(rotation.y), glm::vec3(0, 1, 0));
			};
			break;
	}
}

std::string NdRotate::str() const
{
	std::stringstream ss;
	ss << "Rotate " << _name << " (" << vector.x << "*, " << vector.y << "*, " << vector.z << "*)";
	return ss.str();
}

// Not properly implemented, convert string to lowercase
RotationOrder RotOrderFromString(const std::string& str)
{
	if (str == "xyz")
		return RotationOrder::XYZ;
	if (str == "zxy")
		return RotationOrder::ZXY;
	
	return RotationOrder::XYZ;
}
