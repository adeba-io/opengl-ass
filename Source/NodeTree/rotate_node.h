// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once
#include <glm/glm.hpp>
#include "node.h"
#include "debug"

enum class RotationOrder { XYZ, ZXY };

class NdRotate : public MxnNode, public MxnVector
{
public:
	NdRotate(const char *name, glm::vec3 rotation, RotationOrder order,const fs::path& animPath, const std::vector<MxnNode*>& children = {}, const std::vector<MxnScript*>& scripts = {});

	glm::mat4 GetMatrix() const override { return _fnComputeMatrix(vector); }
	std::string str() const override;

private:
	glm::mat4 (*_fnComputeMatrix)(const glm::vec3& rotation);
};

RotationOrder RotOrderFromString(const std::string& str);
