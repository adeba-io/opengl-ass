// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "node.h"

class NdScale : public MxnNode, public MxnVector
{
public:
	NdScale(const char *name, glm::vec3 scale,const fs::path& animPath, const std::vector<MxnNode*>& children = {}, const std::vector<MxnScript*>& scripts = {})
		: MxnNode(name, animPath, children, scripts, TransformationType::Scale), MxnVector(scale) {}
	
	glm::mat4 GetMatrix() const override { return glm::scale(glm::mat4(1.f), glm::vec3(1.f / vector.x, 1.f / vector.y, 1.f / vector.z)); }
	
	std::string str() const override
	{
		std::stringstream ss;
		ss << "Scale " << _name << " (" << vector.x << ", " << vector.y << ", " << vector.z << ")";
		return ss.str();
	}
};
