// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "core"
#include "node.h"

class NdTranslate : public MxnNode, public MxnVector
{
public:
	NdTranslate(const char *name, glm::vec3 translation, const fs::path& animPath, const std::vector<MxnNode*>& children = {}, const std::vector<MxnScript*>& scripts = {})
		: MxnNode(name, animPath, children, scripts, TransformationType::Translate), MxnVector(translation) {}
	
	glm::mat4 GetMatrix() const override { return glm::translate(glm::mat4(1.f), glm::vec3(vector.x, -vector.y, vector.z)); }

	std::string str() const override
	{
		std::stringstream ss;
		ss << "Translate " << _name << " (" << vector.x << ", " << vector.y << ", " << vector.z << ")";
		return ss.str();
	}
};
