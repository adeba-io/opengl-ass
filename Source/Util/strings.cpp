// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include "strings.h"
#include "debug"

std::vector<std::string> SplitString(const std::string& source, char delimiter)
{
	std::vector<std::string> split;
	int start = 0;
	for (unsigned int i = 0; i < source.size(); i++)
	{
		if (source[i] != delimiter)
			continue;

		if (start == i) continue;

		split.push_back(source.substr(start, i - start));
		start = ++i;
	}
	split.push_back(source.substr(start));

	return split;
}

void TrimLine(std::string& line)
{
	for (unsigned int i = 0; i < line.size(); i++)
	{
		if (line[i] == '\t' || line[i] == ' ')
			continue;
		
		line = line.substr(i);
		return;
	}
}
