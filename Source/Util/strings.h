// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#pragma once

#include "core"

// Splitting only by a char for now
std::vector<std::string> SplitString(const std::string& source, char delimiter);

void TrimLine(std::string& line);
