// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#ifndef COMP_PCH
#pragma once
#endif

#include <fstream>
#include <iostream>
#include <functional>
#include <filesystem>
namespace fs = std::filesystem;

// Data structures
#include <string>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#ifdef PLT_WINDOWS
	#include <windows.h>
#endif
