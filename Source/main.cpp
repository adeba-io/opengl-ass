// I declare that this code is my own work
// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk

#include <GLFW/glfw3.h>
#include <imgui.h>

#include "core"
#include "app"
#include "debug"
#include "input"
#include "gameloop"
#include "nodetree"
#include "render"

#include "Assets/assets.h"
#include "Assets/scene_reader.h"
#include "Debug/imgui_layer.h"
#include "Graphics/Data/texture.h"
#include "Assets/stb_image.h"

static bool OnWindowClose(EvtWindowClose e)
{
	App::SetRunning(false);
	return true;
}

static void OnEvent(MxnEvent& e)
{
	EventDispatcher dispatcher(e);
	dispatcher.Dispatch<EvtWindowClose>(BIND_EVT_FN(OnWindowClose));

	for (auto it = App::GetLayerStack().end(); it != App::GetLayerStack().begin(); )
	{
		(*--it)->OnEvent(e);
		if (e.handled)
			break;
	}
}

int main(void)
{
	Log::Init();
	App::Init();
	Time::Init();
	MngInput::Init();
	Renderer::Init();

	App::GetWindow().SetEventCallback(BIND_EVT_FN(OnEvent));
	stbi_set_flip_vertically_on_load(true);

	MxnNode *treeRoot = ReadSceneData(AssetPath(scene.scn));

	NdCamera *freeCamera = (NdCamera*) FindByNameInTree(*treeRoot, "free_cam");
	NdCamera *roboCamera = (NdCamera*) FindByNameInTree(*treeRoot, "robo_cam");

	App::SetRunning(true);
	App::GetImGUILayer().treeRoot = treeRoot;

	RenderAPI::SetClearColour({ 0.1f, 0.3f, 0.5f, 1 });

	while (App::IsRunning())
	{
		RenderAPI::Clear();
		Time::Update();
		MngInput::Update();

		if (IsDown(MngInput::GetKeyState(GLFW_KEY_ESCAPE)))
			App::SetRunning(false);

		if (IsDown(MngInput::GetKeyState(GLFW_KEY_Z)))
			App::ToggleMouseCapture();


		Renderer::BeginScene(App::IsUsingRobotCamera() ? *roboCamera : *freeCamera);
		TraverseNodeTree(*treeRoot);
		Renderer::EndScene();

		App::GetImGUILayer().Begin();
		
		ImGui::Begin("Controls");

		if (ImGui::Button("Reload"))
		{
			DeleteTree(treeRoot);
			treeRoot = ReadSceneData(AssetPath(scene.scn));
			freeCamera = (NdCamera*) FindByNameInTree(*treeRoot, "free_cam");
			roboCamera = (NdCamera*) FindByNameInTree(*treeRoot, "robo_cam");
			App::GetImGUILayer().treeRoot = treeRoot;
			goto postImGuiTree;
		}
		ImGui::Separator();

		for (MxnLayer* layer : App::GetLayerStack())
			layer->OnImGUIRender();

postImGuiTree:
		ImGui::End();
		App::GetImGUILayer().End();

		App::GetWindow().OnUpdate();
	}

	DeleteTree(treeRoot);
	
	LG_INF("Fin");
	return 1;
}
