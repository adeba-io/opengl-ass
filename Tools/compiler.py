import os
import subprocess
import shlex
from playsound import playsound

import Tools.timer as Timer

ASSET_PATH = '.\\Tools\\assets\\'

ALWAYS = 0
IF_SUCCESS = 1
IF_FAIL = -1
NEVER = 2

sfx_success = "comp_success.wav"
sfx_fail = "comp_fail.wav"

def execute(command, timing = False, play_sound = ALWAYS):
	args = []
	if isinstance(command, str):
		command  = command.replace('\\', '/')
		args = shlex.split(command)
	elif isinstance(command, list):
		args = command
	else:
		print(f"A command of type {type(command)} is not supported")
		return
	
	# print("	" + command)
	
	timer = Timer.Timer()
	if timing:
		timer.start()
	
	# os.system(command)
	output = subprocess.run(args)

	# timer.stop()
	# timer.print()
	
	audio(output.returncode, play_sound)

	return output.returncode == 0

def audio(return_code, play_sound=ALWAYS):
	if play_sound == NEVER:
		return
	
	if return_code == 0:
		if play_sound > -1: # ALWAYS and IF_SUCCESS
			playsound(ASSET_PATH + sfx_success, True)

	if return_code != 0:
		if play_sound < 1: # ALWAYS and IF_FAIL
			playsound(ASSET_PATH + sfx_fail, True)
