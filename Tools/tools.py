import os


profile = {
	'includes': [ 'Source', 'Source/_Includes' ],
	'macros': [ 'GLEW_STATIC', 'SPDLOG_COMPILED_LIB', 'GLFW_INCLUDE_NONE', 'BLD_DEBUG', 'SPDLOG_ACTIVE_LEVEL=0' ],
	'libs': [ 'gdi32' ],
	'options': [ '-g' ]
}

# Funcs
def check_if_in_target_srcs(ext, f):
	srcs = ext['target_srcs']
	if len(srcs) == 0:
		return True
	
	for target_src in ext['target_srcs']:
		if target_src.startswith('*'):
			if f.startswith(target_src[1:]):
				return True
		elif target_src.endswith('*'):
			if f.endswith(target_src[:-1]):
				return True
		elif '*' in target_src:
			print("Wild cards in the middle of the phrase not yet implemented")
		elif f == target_src:
			return True

	return False

def to_compiler_format(profile: dict) -> dict:
	result = {}
	
	option_decorator = lambda key, p, s : [ p + opt + s for opt in profile[key]]
	decorators_switch = {
		'includes': [ '-I', '' ],
		'macros': [ '-D', '' ],
		'libs': [ '-l', '' ]
	}

	for key in profile:
		if type(profile[key]) is not list:
			if key == 'c_std':
				result[key] = '-std=' + profile[key]
			else:
				result[key] = profile[key]
			continue
		
		if key == 'target_srcs':
			result[key] = profile[key]
			continue

		decs = decorators_switch.get(key, [ '', '' ])

		items = list(dict.fromkeys(profile[key]))
		result[key] = ' '.join([ decs[0] + opt + decs[1] for opt in items])

	return result

def from_config_file(file_path: str) -> dict:
	data = {}

	with open(file_path) as file:
		current = ''
		for line in file:
			if '\n' in line:
				line = line[:-1]
			
			if len(line) == 0 or line[0] == '#' or line[0] == ';':
				continue
			
			if line[0] == '$':
				current = line.split(' ', 1)[1].lower()
				data[current] = {}
				continue
			
			if 'l=' in line:
				splits = line.split('l=', 1)
				name = splits[0]
				if name[-1] == ' ':
					name = name[:-1]

				items = splits[1].split(',')
				for i in range(len(items)):
					if len(items[i]) != 0 and items[i][0] == ' ':
						items[i] = items[i][1:]
				
				if '' in items:
					items.remove('')

				data[current][name] = items
			elif '=' in line:
				splits = line.split('=', 1)
				name = splits[0]
				if name[-1] == ' ':
					name = name[:-1]
				
				value = splits[1]
				if value[0] == ' ':
					value = value[1:]
				
				data[current][name] = value
			else:
				data[current][line] = '---'
	
	return data

def read_build_profiles():
	global BLD_DEFAULT
	global BUILD_PROFILES

	data = from_config_file('build_profiles')

	settings = data['settings']

	data.pop('settings')

	profile_keys = list(data.keys())
	if len(profile_keys) < 1:
		print("No Build Profiles Found")
		return

	universal = profile_keys[0]
	profile_keys.remove(universal)
	BUILD_PROFILES[universal] = data[universal]
	
	for key in profile_keys:
		BUILD_PROFILES[key] = build_profiles.combine_build_profiles([ data[key], BUILD_PROFILES[universal] ])
	
	defaults = settings['default']
	BLD_DEFAULT = build_profiles.combine_build_profiles([ BUILD_PROFILES[key] for key in BUILD_PROFILES if key in defaults])

def read_externals():
	ext_data = {}
	cfg_data = from_config_file(f"2Externals\\externals_config")

	prop_names = [ 'compiler', 'src', 'includes', 'macros', 'options', 'c_std', 'libs', 'target_srcs', 'header_only' ]

	for name in cfg_data:
		ext = cfg_data[name]
		new_ext = {}

		for key in prop_names:
			data = ext[key] if key in ext else ''
			
			if data == '':
				if key == 'compiler':
					data = 'g++'
				elif key == 'c_std':
					data = 'c++17'
			
			if key == 'src' or key == 'includes' or key == 'libs' or key == 'macros' or key == 'target_srcs':
				if type(data) is not list:
					data = [ data ]
				
				if '' in data:
					data.remove('')
				
				if key == 'src' or key == 'includes':
					if data == []:
						data = [ f"2Externals/{name}" ]
					else:
						data = [ f"2Externals/{name}{'/' + i if i !=  '.' else ''}" for i in data ]

			new_ext[key] = data
		
		ext_data[name] = new_ext
	return ext_data
