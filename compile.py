import os
import Tools.compiler as compiler
import Tools.tools as tools

ext = tools.read_externals()

profile = tools.profile

for ext in ext.values():
	profile['libs'] = ext['libs'] + profile['libs']
	profile['includes'] = profile['includes'] + ext['includes']

if os.path.exists('Libs'):
	profile['libs'] = \
		[ f.replace('lib', '').replace('.a', '')\
		for f in os.listdir('Libs')\
		if os.path.isfile(f"Libs/{f}") ]\
		+ profile['libs']

cpp_files = []
for root, dir, files in os.walk('Source'):
	to_include = False
	for f in files:
		if f.endswith('.cpp'):
			to_include = True
			break
	else:
		continue
	
	cpp_files.append(f"{root}/*.cpp")

profile = tools.to_compiler_format(profile)

print("Compiling Core")
compiler.execute(\
	f"g++ --std=c++1z -o Museum.exe {' '.join(cpp_files)} " +\
	f"{profile['includes']} -LLibs {profile['libs']} {profile['macros']} {profile['options']}"
)
print("Finished")
