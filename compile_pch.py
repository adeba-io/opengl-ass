import os
import Tools.compiler as compiler
import Tools.tools as tools

profile = tools.to_compiler_format(tools.profile)
compiler.execute(f"g++ --std=c++1z Source/_Includes/pch.h -o Source/pch.h.gch {profile['macros']} -DCOMP_PCH -municode", play_sound=compiler.IF_FAIL) # -Wall -municode {build_profile['options']}")

# def pre_compile_headers(build_profile):
# 	dest_folder = f"{globals.DIR_PRE_COMP}"
# 	if not os.path.exists(dest_folder):
# 		os.makedirs(dest_folder)

# 	input_file = 'pch.h'

# 	with open(f"{dest_folder}/{input_file}", 'w') as output:
# 		output.write("#error Not using GCH")
