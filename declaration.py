# A small script to add the declaration to my files
import os

for root, dir, files in os.walk('Source'):
	for file in files:
		if file.startswith('stb_image') or file == "pch.h.gch":
			continue

		filename = root + '\\' + file
		contents = ''
		with open(filename, 'r') as f:
			contents = f.read()
		
		with open(filename, 'w') as f:
			f.write('// I declare that this code is my own work\n// Author Oluwatamilore Adebayo oaadebayo1@sheffield.ac.uk\n\n')
			f.write(contents)

