### C++ R strings

Declare a string with R"( ... )" and it will allow you to type on multiple lines
R"(
	Hello
	Line 2
)"

The newline char after 'Hello' is preserved

Wavefront Obj file format faces:
`8/1/1 6/2/1 1/3/1` means vertex 1 has position 8, uv 1, normal 1; vertex 2 has position 6, uv, 2, normal 1
